import { GraphQLError } from 'graphql';
import { Node } from './Node';
import { Integer } from './Integer';
import { GlobalId, GlobalIdCast, KeySignature } from './GlobalId';

/**
 * Implement your method arguments with this interface
 * to make it clear that the field returns connection type.
 */
export interface ConnectionArguments {
  first?: Integer;
  after?: string;
  last?: Integer;
  before?: string;
}

export type ConnectionEdgeFields = 'cursor' | 'node';

export interface ConnectionPageInfo {
  startCursor?: string;
  endCursor?: string;
  hasNextPage?: boolean;
  hasPreviousPage?: boolean;
}

export interface ConnectionEdge<T extends Node = Node> {
  cursor: string;
  node: T;
}

export type ConnectionFields = 'edges' | 'pageInfo';

export interface Connection<T extends Node = Node, E extends ConnectionEdge<T> = ConnectionEdge<T>, P extends ConnectionPageInfo = ConnectionPageInfo> {
  edges: E[];
  nodes: T[];
  pageInfo: P;
}

export interface ConnectionArraySliceMetaInfo {
  sliceStart: number;
  arrayLength: number;
}

export abstract class CursorId<S extends KeySignature> extends GlobalId<S> {
  static entityName = 'cursor';
}

const cursorType = 'ArrayConnection';
/**
 * Creates the cursor string from an offset.
 */
function offsetToCursor(offset: number): string {
  return CursorId.stringify(cursorType, 'Integer', offset);
}

/**
 * Rederives the offset from the cursor string.
 */
function cursorToOffset(cursor: string): number {
  return CursorId.parse(cursor, 'Integer', cursorType);
}

/**
 * Given an optional cursor and a default offset, returns the offset
 * to use; if the cursor contains a valid offset, that will be used,
 * otherwise it will be the default.
 */
function getOffsetWithDefault(
  cursor: string | void,
  defaultOffset: number,
): number {
  if (typeof cursor !== 'string') {
    return defaultOffset;
  }
  const offset = cursorToOffset(cursor);
  return isNaN(offset) ? defaultOffset : offset;
}

export function connectionFromArraySlice<T extends Node>(
  nodes: T[],
  args: ConnectionArguments,
  meta: ConnectionArraySliceMetaInfo,
): Connection<T> {
  const { after, before, first, last } = args;
  const { sliceStart, arrayLength } = meta;
  const sliceEnd = sliceStart + nodes.length;
  const beforeOffset = getOffsetWithDefault(before, arrayLength);
  const afterOffset = getOffsetWithDefault(after, -1);

  let startOffset = Math.max(
    sliceStart - 1,
    afterOffset,
    -1
  ) + 1;

  let endOffset = Math.min(
    sliceEnd,
    beforeOffset,
    arrayLength
  );

  if (typeof first === 'number') {
    if (first < 0) {
      throw new GraphQLError('Argument "first" must be a non-negative integer');
    }

    endOffset = Math.min(
      endOffset,
      startOffset + first
    );
  }

  if (typeof last === 'number') {
    if (last < 0) {
      throw new GraphQLError('Argument "last" must be a non-negative integer');
    }

    startOffset = Math.max(
      startOffset,
      endOffset - last
    );
  }

  // If supplied slice is too large, trim it down before mapping over it.
  const slice = nodes.slice(
    Math.max(startOffset - sliceStart, 0),
    nodes.length - (sliceEnd - endOffset)
  );

  const lowerBound = after ? (afterOffset + 1) : 0;
  const upperBound = before ? beforeOffset : arrayLength;
  const pageInfo = {
    hasPreviousPage:
      typeof last === 'number' ? startOffset > lowerBound : false,
    hasNextPage:
      typeof first === 'number' ? endOffset < upperBound : false,
  };
  const mapEdge = (node: any, index: number) => ({
    node,
    cursor: offsetToCursor(index),
  });

  return connection(slice, mapEdge, pageInfo);
}


/**
 * A simple function that accepts an array and connection arguments, and returns
 * a connection object for use in GraphQL. It uses array offsets as pagination,
 * so pagination will only work if the array is static.
 */
export function connectionFromArray<T extends Node>(
  nodes: T[],
  args: ConnectionArguments,
): Connection<T> {
  return connectionFromArraySlice(
    nodes,
    args,
    {
      sliceStart: 0,
      arrayLength: nodes.length,
    },
  );
}

export interface ConnectionParsedArgsWithoutPosition {
  order: 'first' | 'last';
  limit: Integer;
  pos: undefined;
  from: undefined;
}

export interface ConnectionParsedArgsWithPosition {
  order: 'first' | 'last';
  limit: Integer;
  pos: 'before' | 'after';
  from: string;
}

export type ConnectionParsedArgs =
  ConnectionParsedArgsWithoutPosition
  | ConnectionParsedArgsWithPosition;

export interface ParseConnectionArgsOptions {
  requiredLimit?: boolean;
  defaultLimit?: Integer;
  minLimit?: Integer;
  maxLimit?: Integer;
  defaultOrder?: 'first' | 'last';
}

export function parseConnectionArgs(
  args: ConnectionArguments,
  options: ParseConnectionArgsOptions = {},
): ConnectionParsedArgs {
  let order: 'first' | 'last' = typeof options.defaultOrder !== 'undefined'
    ? options.defaultOrder
    : 'first';
  let pos: 'before' | 'after' | undefined;
  let limit: Integer | undefined;
  let from: string | undefined;

  if (typeof args.first !== 'undefined' && typeof args.last !== 'undefined') {
    throw new GraphQLError(`You must choose "first" or "last"`);
  }

  if (typeof args.first === 'number') {
    if (args.first < 1) {
      throw new GraphQLError(`Argument "first" must be greater than 0`);
    }
    order = 'first';
    limit = args.first;
  } else if (typeof args.last === 'number') {
    order = 'last';
    limit = args.last;
    if (args.last < 1) {
      throw new GraphQLError(`Argument "last" must be greater than 0`);
    }
  }

  if (typeof limit === 'undefined') {
    limit = options.defaultLimit;
  }

  if (limit == null) {
    if (options.requiredLimit === true) {
      throw new GraphQLError(`You must specify "first" or "last" arguments`);
    }
    limit = 0;
  }
  if (typeof options.minLimit !== 'undefined' && limit < options.minLimit) {
    throw new GraphQLError(`The "${order}" argument must be greater or equal "${options.minLimit}"`);
  } else if (typeof options.maxLimit !== 'undefined' && limit > options.maxLimit) {
    throw new GraphQLError(`The "${order}" argument must be greater or equal "${options.maxLimit}"`);
  }

  if (typeof args.before === 'string') {
    pos = 'before';
    from = args.before;
  } else if (typeof args.after === 'string') {
    pos = 'after';
    from = args.after;
  } else {
    return {
      order,
      limit,
      pos: undefined,
      from: undefined,
    };
  }

  return { pos, order, limit, from };
}

/**
 * A simple function that accepts an array and connection arguments, and returns
 * a connection object for use in GraphQL. It uses array offsets as pagination,
 * so pagination will only work if the array is static.
 */
export function connection<T extends Node>(
  nodes: T[],
  mapEdge: (node: T, index: number) => ConnectionEdge<T>,
  pageInfo: ConnectionPageInfo,
): Connection<T> {
  const edges = nodes.map((node, index) => mapEdge(node, index));
  return {
    nodes,
    edges,
    pageInfo: {
      ...(edges.length > 0
        ? {
          startCursor: edges[0].cursor,
          endCursor: edges[edges.length - 1].cursor,
        }
        : null
      ),
      ...pageInfo,
    },
  };
}