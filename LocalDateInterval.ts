import { Interval } from 'luxon';
export type LocalDateInterval = Interval;
