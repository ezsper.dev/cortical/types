import { GraphQLError } from 'graphql';
import { GraphQLEmail } from '../../../graphql';
declare var jest, describe, it, expect;

describe('GraphQLEmail', () => {

  it('parseValue output booleans', () => {
    expect(GraphQLEmail.parseValue('example@gmail.com')).toBe('example@gmail.com');
  });

  it('throws when invalid', () => {
    expect(() => GraphQLEmail.parseValue('')).toThrowError(GraphQLError);
    expect(() => GraphQLEmail.parseValue('example@')).toThrowError(GraphQLError);
  })
});
