import { ApolloError } from 'apollo-server-errors';

interface InvalidIDErrorOptions {
  method: string;
  message?: string;
  id?: string;
  data?: any;
  matchType?: string[];
  encoding?: string;
}

export class InvalidIDError extends ApolloError {
  constructor(options: InvalidIDErrorOptions) {
    const { message, ...properties } = options;
    super(
      message == null ? `Invalid global id` : message,
      'INVALID_ID_ERROR',
      properties,
    );
  }
}