import { ID } from './ID';
import { Long } from './Long';
import { Uuid } from './Uuid';
import { TimeUuid } from './TimeUuid';
import { URI } from './URI';
import { Email } from './Email';
import { Phone } from './Phone';
import { DateTime } from './DateTime';
import { InvalidIDError } from './errors/InvalidIDError';

export interface GlobalIdCastRequired {
  'Uuid': Uuid;
  'TimeUuid': TimeUuid;
  'DateTime': DateTime;
  'LocalDate': DateTime;
  'Long': Long;
  'String': string;
  'Integer': number;
  'Email': Email;
  'URI': URI;
  'Phone': Phone;
  'Float': number;
  'Blob': Buffer;
}

export interface GlobalIdCastOptional {
  'Uuid?': Uuid | void;
  'TimeUuid?': TimeUuid | void;
  'DateTime?': DateTime | void;
  'LocalDate?': DateTime | void;
  'Long?': Long | void;
  'String?': string | void;
  'Integer?': number | void;
  'Email?': Email | void;
  'URI?': URI | void;
  'Phone?': Phone | void;
  'Float?': number | void;
  'Blob?': Buffer;
}

export interface GlobalIdCast extends GlobalIdCastRequired, GlobalIdCastOptional {}

export type IdValue = Uuid | TimeUuid | DateTime | Long | string | number | Buffer;
export type DataId = { [key: string]: IdValue | void };

export type GlobalIdValue<T extends GlobalIdCast[keyof GlobalIdCast] | Buffer> = { type: string, id: T };

const ESCAPE = (new Buffer('\\'))[0];
const SEPARATOR = (new Buffer(';'))[0];

export type KeySignature = { [key: string]: keyof GlobalIdCast };
export type Key = keyof GlobalIdCastRequired | KeySignature;

/**
 * Helper library to manipulate Relay's global id
 */
export class GlobalId<S extends { [key: string]: keyof GlobalIdCast }>  {

  static entityName = 'node';

  /**
   * Signs a GlobalId entity
   */
  static sign<S extends KeySignature>(entityName: string, signature: S): GlobalId<S> {
    return new GlobalId(entityName, signature);
  }

  static isValid(value: string, encoding = 'base64') {
    const buffer = new Buffer(value, encoding);
    if (buffer.length > 5) {
      const typeSize = parseInt(buffer.slice(0, 3).toString(), 10);
      if (buffer.slice(0, 3).toString() === `00${typeSize}`.slice(-3)
        && buffer.length > (typeSize + 4)) {
        return true;
      }
    }
    return false;
  }

  static validate(id: string, entityName = this.entityName, encoding = 'base64') {
    if (!GlobalId.isValid(id, encoding)) {
      throw new InvalidIDError({
        id,
        encoding,
        matchType: [entityName],
        method: 'parse',
      });
    }
    return true;
  }

  static stringify<D extends { [key: string]: keyof GlobalIdCast }>(type: string, Key: D, id: { [K in keyof D]: GlobalIdCast[D[K]] }, encoding?: string): ID;
  static stringify<K extends keyof GlobalIdCastRequired>(type: string, Key: K, id: GlobalIdCastRequired[K], encoding?: string): ID;
  static stringify(type: string, Key: Key, id: IdValue | DataId, encoding = 'base64'): ID {
    const typeBuf: Buffer = new Buffer(type);
    if (typeBuf.length > 999) {
      throw new InvalidIDError({
        encoding,
        message: `The type argument encodes more than 999 bytes`,
        matchType: [type],
        data: id,
        method: 'stringify',
      });
    }
    if (typeBuf.length === 0) {
      throw new InvalidIDError({
        encoding,
        message: `The type argument encodes 0 bytes`,
        matchType: [type],
        data: id,
        method: 'stringify',
      });
    }
    const stringify = (value: IdValue | void): Buffer => {
      let buffer: Buffer;
      if (typeof value === 'undefined') {
        buffer = new Buffer(0);
      } else if (value instanceof Uuid) {
        buffer = value.getBuffer();
      } else if (value instanceof Long || typeof value === 'number') {
        buffer = new Buffer(value.toString());
      } else if (typeof value === 'string') {
        buffer = new Buffer(value);
      } else if (value instanceof Buffer) {
        buffer = value;
      } else if (value instanceof DateTime) {
        const iso = value.toISO();
        if (iso == null) {
          throw new InvalidIDError({
            encoding,
            message: `DateTime is not valid`,
            matchType: [type],
            data: id,
            method: 'stringify',
          });
        }
        buffer = new Buffer(iso);
      } else {
        throw new InvalidIDError({
          encoding,
          message: `Couldn't stringify`,
          matchType: [type],
          data: id,
          method: 'stringify',
        });
      }
      if (typeof value !== 'undefined' && buffer.length === 0) {
        throw new InvalidIDError({
          encoding,
          message: `Data encodes 0 bytes`,
          matchType: [type],
          data: id,
          method: 'stringify',
        });
      }
      let cleanedBuffer = new Buffer(0);
      let offset = 0;
      for (const index of buffer.keys()) {
        if (buffer[index] === SEPARATOR && buffer[index - 1] !== ESCAPE) {
          cleanedBuffer = Buffer.concat([
            cleanedBuffer,
            buffer.slice(offset, index),
            new Buffer([ESCAPE]),
            new Buffer([SEPARATOR]),
          ]);
          offset = index + 1;
        }
      }
      if (offset < buffer.length) {
        cleanedBuffer = Buffer.concat([
          cleanedBuffer,
          buffer.slice(offset, buffer.length),
        ]);
      }
      return cleanedBuffer;
    };
    const buffers: Buffer[] = [
      new Buffer(`00${typeBuf.length}`.slice(-3)),
      typeBuf,
      new Buffer(':'),
    ];
    if (typeof Key === 'string') {
      buffers.push(stringify(<any>id));
    } else {
      const keys = Object.keys(Key);
      while (keys.length) {
        const key = keys.shift();
        if (typeof key === 'undefined') {
          break;
        }
        buffers.push(stringify((<any>id)[key]));
        if (keys.length > 0) {
          buffers.push(new Buffer([SEPARATOR]));
        }
      }
    }
    return Buffer.concat(buffers).toString(encoding);
  }

  static stringifyValues<D extends { [key: string]: keyof GlobalIdCast }>(type: string, Key: D, id: { [K in keyof D]: GlobalIdCast[D[K]] }, encoding?: string): string;
  static stringifyValues<K extends keyof GlobalIdCastRequired>(type: string, Key: K, id: GlobalIdCastRequired[K], encoding?: string): string;
  static stringifyValues(type: string, Key: Key, id: IdValue | DataId, encoding = 'base64'): string {
    const decoded = GlobalId.stringify(type, <any>Key, id, 'utf8');
    const [ entityName, ...rest ] = decoded.split(':');
    return new Buffer(rest.join(':')).toString(encoding);
  }

  static getType(globalId: string, encoding = 'base64'): string {
    GlobalId.validate(globalId, undefined, encoding);
    const buffer = new Buffer(globalId, encoding);
    const typeSize = parseInt(buffer.slice(0, 3).toString(), 10);
    return buffer.slice(3, 3 + typeSize).toString();
  }

  static parse<K extends keyof GlobalIdCast>(globalId: ID, cast: K, matchType?: string | string[], encoding?: string): GlobalIdCast[K];
  static parse<D extends { [key: string]: keyof GlobalIdCast }>(globalId: ID, cast: D, matchType?: string, encoding?: string): { [K in keyof D]: GlobalIdCast[D[K]] };
  static parse(globalId: ID, cast: Key, matchType?: string, encoding = 'base64'): any {
    const parseValue = (blob: Buffer, cast: keyof GlobalIdCast) => {
      if (blob.length === 0) {
        if (cast.match(/\?$/) == null) {
          throw new InvalidIDError({
            encoding,
            ...(matchType != null && {
              matchType: Array.isArray(matchType) ? matchType : [matchType],
            }),
            message: `Empty data for id field`,
            id: globalId,
            method: 'parse',
          });
        }
        return undefined;
      }
      switch (cast.replace(/\?$/, '')) {
        case 'Uuid':
          return new Uuid(blob);
        case 'TimeUuid':
          return new TimeUuid(blob);
        case 'DateTime':
        case 'LocalDate':
          return DateTime.fromISO(blob.toString());
        case 'Long':
          return Long.fromString(blob.toString());
        case 'String':
        case 'URI':
        case 'Email':
        case 'Phone':
          return blob.toString();
        case 'Integer':
          return parseInt(blob.toString(), 10);
        case 'Float':
          return parseFloat(blob.toString());
        case 'Blob':
          return blob;
        default:
          throw new InvalidIDError({
            encoding,
            ...(matchType != null && {
              matchType: Array.isArray(matchType) ? matchType : [matchType],
            }),
            message: `Unknown cast`,
            id: globalId,
            method: 'parse',
          });
      }
    };
    const buffer = new Buffer(globalId, encoding);
    const typeSize = parseInt(buffer.slice(0, 3).toString(), 10);
    if (typeof matchType !== 'undefined') {
      const type = this.getType(globalId, encoding);
      if ((Array.isArray(matchType) && matchType.indexOf(type) < 0) || type !== matchType) {
        throw new InvalidIDError({
          encoding,
          ...(matchType != null && {
            matchType: Array.isArray(matchType) ? matchType : [matchType],
          }),
          message: `Global id type doesn't match with type`,
          id: globalId,
          method: 'parse',
        });
      }
    } else {
      this.validate(globalId, matchType, encoding);
    }
    const blob = buffer.slice(typeSize + 4);
    if (typeof cast === 'undefined') {
      return blob;
    }
    if (typeof cast === 'string') {
      return parseValue(blob, cast);
    }
    const obj: any = {};
    const keys = Object.keys(cast);
    let valueCount = 0;
    let offset = 0;
    const nextValue = (index: number) => {
      const key = keys.shift();
      if (typeof key === 'undefined') {
        throw new InvalidIDError({
          encoding,
          ...(matchType != null && {
            matchType: Array.isArray(matchType) ? matchType : [matchType],
          }),
          message: `Too much values`,
          id: globalId,
          method: 'parse',
        });
      }
      valueCount = valueCount + 1;
      obj[key] = parseValue(blob.slice(offset, index), cast[key]);
      offset = index + 1;
    };
    for (const index of blob.keys()) {
      const byte = blob[index];
      if (byte === SEPARATOR && blob[index - 1] !== ESCAPE) {
        nextValue(index);
      }
    }
    nextValue(blob.length);
    if (valueCount < keys.length) {
      throw new InvalidIDError({
        encoding,
        ...(matchType != null && {
          matchType: Array.isArray(matchType) ? matchType : [matchType],
        }),
        message: `Too little values`,
        id: globalId,
        method: 'parse',
      });
    }
    return obj;
  }

  static parseValues<K extends keyof GlobalIdCast>(strValues: string, cast: K, matchType?: string, encoding?: string): GlobalIdCast[K];
  static parseValues<D extends { [key: string]: keyof GlobalIdCast }>(strValues: string, cast: D, matchType?: string, encoding?: string): { [K in keyof D]: GlobalIdCast[D[K]] };
  static parseValues(strValues: string, cast: Key, matchType: string = 'Unknown', encoding = 'base64'): any {
    const typeSize = `00${matchType.length}`.slice(-3);
    const prefix = new Buffer(`${typeSize}${matchType}:`);
    const buffer = Buffer.concat([prefix, new Buffer(strValues, encoding)]);
    return GlobalId.parse(buffer.toString('utf8'), <any>cast, matchType === 'Unknown' ? undefined : matchType, 'utf8');
  }

  constructor(public entityName: string, public signature: S) {}

  stringify(data: { [K in keyof S]: GlobalIdCast[S[K]] }, encoding = 'base64'): ID {
    return GlobalId.stringify(this.entityName, this.signature, data, encoding);
  }

  stringifyValues(data: { [K in keyof S]: GlobalIdCast[S[K]] }, encoding = 'utf8'): string {
    return GlobalId.stringifyValues(this.entityName, this.signature, data, 'utf8');
  }

  parse(id: ID, encoding = 'base64'): { [K in keyof S]: GlobalIdCast[S[K]] } {
    return GlobalId.parse(id, this.signature, this.entityName, encoding);
  }

  parseValues(strValues: string, encoding = 'utf8'): { [K in keyof S]: GlobalIdCast[S[K]] } {
    return GlobalId.parseValues(strValues, this.signature, this.entityName, encoding);
  }

}
