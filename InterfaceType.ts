import {
  GraphQLResolveInfo,
  GraphQLObjectType,
} from 'graphql';

function lowerFirst(value: string) {
  return value.charAt(0).toLowerCase() + value.slice(1)
}

// Fixes 'extends' clause of exported class '*' has or is using private name 'ObjectTypeStatic'

declare global {
  export interface ObjectTypeStatic<F extends {}> {
    graphqlType: GraphQLObjectType;
    forge(data: F): F & ObjectTypeMember<F>;
    isTypeOf(value: any, context: any, info: GraphQLResolveInfo): boolean;
    new (data: F): F & ObjectTypeMember<F>;
  }

  export interface ObjectTypeMember<F extends {}> {
    set<K extends keyof F>(key: K, value: F[K]): this;
    set(data: Partial<F>): this;
    get<K extends keyof F>(key: K): F[K];
    toPlainObject(): Partial<F>;
    toJSON(): Partial<F>;
    toGraph(context: any, info: GraphQLResolveInfo): Partial<F> | Promise<Partial<F>>;
  }
}

export { ObjectTypeStatic, ObjectTypeMember };

// tslint:disable-next-line function-name
export function ObjectType<F>(Fields: { new (): F }): ObjectTypeStatic<F> {
  class AbstractObjectType extends (<({ new(): any; })>Fields) implements ObjectTypeMember<F> {

    static forge(data: F) {
      return new this(data);
    }

    static isTypeOf(value: any, context: any, info: GraphQLResolveInfo) {
      if (value instanceof this) {
        return true;
      }
      if (value.__typename === this.name) {
        return true;
      }
      return false;
    }

    protected data: F;

    set<K extends keyof F>(key: K, value: F[K]): this;
    set(data: Partial<F>): this;
    set(...args: any[]): this {
      if (typeof this.data === 'undefined') {
        this.data = <any>{};
      }
      let data: Partial<F>;
      if (typeof args[0] === 'string') {
        const [key, value] = args;
        data = <any>{ [key]: value };
      } else {
        data = args[0];
      }
      for (const key in data) {
        (<any>this)[key] = data[key];
        Object.defineProperty(this, key, {
          get() {
            return this.data[key];
          },
          set(value) {
            return this.data[key] = value;
          }
        });
      }
      Object.assign(this.data, data);
      return this;
    }

    get<K extends keyof F>(key: K): F[K] {
      return (<any>this).data[key];
    }

    toPlainObject(): F {
      const data: any = {};
      for (const key in this) {
        if (key === 'data') {
          continue;
        }
        data[key] = this[key];
      }
      const descriptions: any = {};
      let proto = this.constructor.prototype;
      while(proto) {
        if (proto === Object.prototype) {
          break;
        }
        Object.assign(descriptions, Object.getOwnPropertyDescriptors(proto));
        proto = Object.getPrototypeOf(proto);
      }
      for (const key in descriptions) {
        if (typeof descriptions[key].get !== 'undefined') {
          data[key] = (<any>this)[key];
        }
      }
      for (const key in this.data) {
        if (typeof this.data[key] !== 'undefined') {
          data[key] = this.data[key];
        }
      }
      return <any>data;
    }

    constructor(data: F) {
      super();
      this.set(data);
      const descriptions: any = {};
      let proto = this;
      while(proto) {
        if (proto === Object.prototype) {
          break;
        }
        Object.assign(descriptions, Object.getOwnPropertyDescriptors(proto));
        proto = Object.getPrototypeOf(proto);
      }
      for (const key in descriptions) {
        const match = key.match(/^resolve([A-Z].+)$/);
        if (match == null) {
          continue;
        }
        if (typeof descriptions[key].value !== 'function') {
          continue;
        }
        const defaultResolver = (<any>this)[key];
        Object.defineProperty(this, key, {
          value: function resolver(...args: any[]) {
            const value = defaultResolver.apply(this, args);
            const resolve = (val: any) => {
              this[lowerFirst(match[1])] = val;
              return val;
            };
            if (value instanceof Promise) {
              return value.then(resolve);
            }
            return resolve(value);
          },
        });
      }
    }

    toJSON(): Partial<F> {
      return this.toPlainObject();
    }

    toGraph(context: any, info: GraphQLResolveInfo): Partial<F> {
      return this.toPlainObject();
    }

  }
  Object.defineProperty(AbstractObjectType, 'name', {
    configurable: true,
    value: `ObjectType<${(<any>Fields).name}>`
  });
  return <any>AbstractObjectType;
}
