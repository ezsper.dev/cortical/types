import * as crypto from 'crypto';

export class Uuid {

  static fromString(value: string) {
    // 36 chars: 32 + 4 hyphens
    if (typeof value !== 'string' || value.length !== 36) {
      throw new TypeError('Invalid string representation of Uuid, it should be in the 00000000-0000-0000-0000-000000000000');
    }
    return new Uuid(new Buffer(value.replace(/-/g, ''), 'hex'));
  }

  static random() {
    const buffer = crypto.randomBytes(16);
    // clear the version
    buffer[6] &= 0x0f;
    // set the version 4
    buffer[6] |= 0x40;
    // clear the variant
    buffer[8] &= 0x3f;
    // set the IETF variant
    buffer[8] |= 0x80;
    return new Uuid(buffer);
  }

  constructor(private buffer: Buffer) {
    if (buffer.length !== 16) {
      throw new TypeError('You must provide a buffer containing 16 bytes');
    }
  }

  getBuffer() {
    return this.buffer;
  }

  equals(uuid: Uuid) {
    return this.toString() === uuid.toString();
  }

  toString(): string {
    const hex = this.buffer.toString('hex');
    return [
      hex.substr(0, 8),
      hex.substr(8, 4),
      hex.substr(12, 4),
      hex.substr(16, 4),
      hex.substr(20, 12),
    ].join('-');
  }

  toJSON() {
    return this.toString();
  }

}
