import {
  GraphQLScalarType,
  GraphQLInterfaceType,
} from 'graphql';
import * as builtinTypes from './graphql';

const externalTypes = require('@cortical/core/hooks/externalTypes').default;

externalTypes.addFilter('types', (types: any) => {
  return [
    ...types,
    ...Object.values(builtinTypes)
      .filter(value => (
        value instanceof GraphQLScalarType
        || value instanceof GraphQLInterfaceType
      )),
  ];
});
