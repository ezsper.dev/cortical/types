import { DateTime } from 'luxon';
export type LocalDate = DateTime;
