import {
  GraphQLScalarType,
  GraphQLList,
  Kind,
  GraphQLError,
} from 'graphql';
import { DateTimeOptions } from 'luxon';
import { DateTime } from '../DateTime';

const millisecondsPerDay = 86400000;

export function parseValue(
  value: any,
  dateOptions?: DateTimeOptions,
  end = false,
): DateTime {
  if (value instanceof DateTime) {
    return value;
  }
  const options = {
    zone: 'UTC',
    locale: 'en-US',
    ...dateOptions,
  };
  if (value instanceof Date) {
    return DateTime.fromJSDate(value, options);
  }
  try {
    if (typeof value === 'string') {
      const isoString = !/(T| )([0-9.:]+)/.test(value)
        ? value.replace(/^([0-9-]+)/, end ? '$1T23:59:59' : '$1T00:00:00')
        : value;
      return DateTime.fromISO(isoString, options);
    } else if (typeof value === 'number') {
      if(value < -2147483648 || value > 2147483647) {
        throw new Error('You must provide a valid value for days since epoch (-2147483648 <= value <= 2147483647).');
      }
      const date = new Date(value * millisecondsPerDay);
      const year = date.getUTCFullYear();
      const month = date.getUTCMonth() + 1;
      const day = date.getUTCDate();
      const { zone, locale } = options;
      return DateTime.fromObject({ year, month, day, zone, locale });
    }
  } catch(error) {
    throw new GraphQLError(error.message);
  }
  throw new GraphQLError('Invalid local date');
}

export function createScalar(dateOptions?: DateTimeOptions) {
  const options = {
    zone: 'UTC',
    locale: 'en-US',
    ...dateOptions,
  };
  const { zone, locale } = options;
  return new GraphQLScalarType({
    name: 'LocalDate',
    description: 'An ISO-8601 encoded UTC date string (without time component)',
    serialize(value: DateTime | string) {
      let date: DateTime;
      if (typeof value === 'string') {
        date = parseValue(value, options);
      } else {
        date = value;
      }
      return date.setZone(zone).setLocale(locale).toISO().replace(/(T| )([0-9.:]+)/, '');
    },
    parseValue: (value) => parseValue(value, options),
    parseLiteral(valueNode) {
      if (valueNode.kind === Kind.STRING) {
        return parseValue(valueNode.value, options);
      }
      return null;
    },
  });
}

export const GraphQLLocalDate = createScalar();
