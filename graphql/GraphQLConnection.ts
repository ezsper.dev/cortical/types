import {
  GraphQLInterfaceType,
  GraphQLNonNull,
  GraphQLList,
} from 'graphql';

import { GraphQLConnectionEdge } from './GraphQLConnectionEdge';
import { GraphQLConnectionPageInfo } from './GraphQLConnectionPageInfo';
import { GraphQLNode } from './GraphQLNode';
import { Connection } from '../Connection';

export const GraphQLConnection = new GraphQLInterfaceType({
  name: 'Connection',
  description: 'The standardized Relay object for dealing with pagination on Graph',
  fields: {
    edges: {
      type: new GraphQLList(GraphQLConnectionEdge),
      description: "The connection edges",
      args: {},
    },
    nodes: {
      type: new GraphQLList(GraphQLNode),
      description: "The connection nodes",
      args: {},
    },
    pageInfo: {
      type: new GraphQLNonNull(GraphQLConnectionPageInfo),
      description: "The connection page info",
      args: {},
    },
  },
});
