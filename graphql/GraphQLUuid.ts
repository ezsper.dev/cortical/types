import {
  GraphQLScalarType,
  GraphQLList,
  Kind,
  GraphQLError,
} from 'graphql';

import { Uuid } from '../Uuid';

function parseValue(value: any): Uuid {
  if (value instanceof Uuid) {
    return value;
  }
  if (typeof value === 'string') {
    try {
      return Uuid.fromString(value);
    } catch(error) {
      throw new GraphQLError(error.message);
    }
  }
  throw new GraphQLError('Invalid uuid');
}

export const GraphQLUuid = new GraphQLScalarType({
  name: 'Uuid',
  description: 'A 128-bit number used to uniquely identify some object or entity',
  serialize(value: Uuid) {
    return value.toString();
  },
  parseValue,
  parseLiteral(valueNode) {
    if (valueNode.kind === Kind.STRING) {
      return parseValue(valueNode.value);
    }
    return null;
  },
});
