import {
  GraphQLScalarType,
  Kind,
  GraphQLError,
} from 'graphql';

const resolveLanguage = (value: string): string | null => {
  let lang: string;
  let country: string | undefined;
  let match = value.match(/^([a-z]{2})([A-Z]{2})$/);
  if (match != null) {
    lang = match[1];
    country = match[2];
  } else {
    match = value.match(/^((?:[a-z]{2}|[A-Z]{2}))(?:(?:-|_)(?:([a-z]{2}|[A-Z]{2})))?$/);
    if (match != null) {
      lang = match[1].toLowerCase();
      country = match[2] != null
        ? match[2].toUpperCase()
        : undefined;
    } else {
      return null;
    }
  }

  if (!require('iso-639-1').validate(lang)) {
    return null;
  }

  if (country != null && !require('i18n-iso-countries').isValid(country)) {
    return null;
  }

  return country == null ? `${lang}` : `${lang}_${country}`;
};

export const GraphQLLanguage = new GraphQLScalarType({
  name: 'Language',
  description: `An ISO-639 Language Code and ISO-3166 Country Code.`,
  serialize (value) {
    if (typeof value !== 'string') {
      return null;
    }
    return resolveLanguage(value);
  },
  parseValue (value) {
    const languageCode = resolveLanguage(value);
    if (languageCode != null) {
      return languageCode;
    }
    throw new GraphQLError('', [])
  },
  parseLiteral (ast) {
    if (ast.kind !== Kind.STRING) {
      throw new GraphQLError(`Type should be "String", found ${ast.kind}.`, [ast])
    }
    const languageCode = resolveLanguage(ast.value);
    if (languageCode != null) {
      return languageCode;
    }
    throw new GraphQLError(`Invalid Language literal.\n${ast.value} is not valid Language.`, [ast])
  }
});
