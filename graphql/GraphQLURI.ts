import {
  GraphQLScalarType,
  Kind,
  GraphQLError,
} from 'graphql';
import * as isURL from 'validator/lib/isURL';

export const GraphQLURI = new GraphQLScalarType({
  name: 'URI',
  description: `An RFC 3986, RFC 3987, and RFC 6570 (level 4) compliant URI string.`,
  serialize (value) {
    if (typeof value !== 'string') {
      return null
    }
    if (isURL(value)) {
      return value
    }
    return null
  },
  parseValue (value) {
    if (typeof value !== 'string') {
      throw new GraphQLError('', [])
    }
    if (isURL(value)) {
      return value
    }
    throw new GraphQLError('', [])
  },
  parseLiteral (ast) {
    if (ast.kind !== Kind.STRING) {
      throw new GraphQLError(`Type should be "String", found ${ast.kind}.`, [ast])
    }
    if (isURL(ast.value)) {
      return ast.value
    }
    throw new GraphQLError(`Invalid URL literal.\n${ast.value} is not URL.`, [ast])
  }
});
