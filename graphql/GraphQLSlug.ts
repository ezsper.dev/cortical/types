import {
  GraphQLScalarType,
  Kind,
  GraphQLError,
} from 'graphql';

function isSlug(input: string) {
  if (/^[a-zA-Z][a-zA-Z0-9-_]{1,30}[a-zA-Z0-9]$/.test(input)) {
    if (input.replace(/(-_)[-_]+/, '$1').length === input.length) {
      return true;
    }
  }
  return false;
}

export const GraphQLSlug = new GraphQLScalarType({
  name: 'Slug',
  description: `An unique string identifier of an entity that is URL friendly.
It expects 3 to 32 charecters, starting with a letter and ending with an alphanumeric value, it accepts hyphens or underscores charecters in place of spaces.
Ex.: title: \`The Chloé O'Connor 1th Song\` slug: \`the-cloe-oconnor-1th-song\`
`,
  serialize (value) {
    if (typeof value !== 'string') {
      return null
    }
    if (isSlug(value)) {
      return value;
    }
    return null
  },
  parseValue (value) {
    if (typeof value !== 'string') {
      throw new GraphQLError('', [])
    }
    if (isSlug(value)) {
      return value;
    }
    throw new GraphQLError('', [])
  },
  parseLiteral (ast) {
    if (ast.kind !== Kind.STRING) {
      throw new GraphQLError(`Type should be "String", found ${ast.kind}.`, [ast])
    }
    if (isSlug(ast.value)) {
      return ast.value;
    }
    throw new GraphQLError(`Invalid Slug literal.\n${ast.value} is not Slug.`, [ast])
  }
});
