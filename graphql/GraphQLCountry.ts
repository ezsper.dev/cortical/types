import {
  GraphQLScalarType,
  GraphQLError,
  Kind,
} from 'graphql';

const resolveCountry = (value: string): string | null => {
  if (value.length === 2) {
    if (require('i18n-iso-countries').alpha2ToAlpha3(value.toUpperCase()) != null) {
      return value.toUpperCase();
    }
  } else if (value.length === 3) {
    return require('i18n-iso-countries').alpha3ToAlpha2(value.toUpperCase());
  }
  return require('i18n-iso-countries').getAlpha2Code(value, 'en') || null;
};

export const GraphQLCountry = new GraphQLScalarType({
  name: 'Country',
  description: `An ISO-3166 Country Code.'`,
  serialize (value) {
    if (typeof value !== 'string') {
      return null;
    }
    return resolveCountry(value);
  },
  parseValue (value) {
    const countryCode = resolveCountry(value);
    if (countryCode != null) {
      return countryCode;
    }
    throw new GraphQLError('', [])
  },
  parseLiteral (ast) {
    if (ast.kind !== Kind.STRING) {
      throw new GraphQLError(`Type should be "String", found ${ast.kind}.`, [ast])
    }
    const countryCode = resolveCountry(ast.value);
    if (countryCode != null) {
      return countryCode;
    }
    throw new GraphQLError(`Invalid Country literal.\n${ast.value} is not valid Country.`, [ast])
  }
});
