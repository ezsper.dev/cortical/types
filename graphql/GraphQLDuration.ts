import {
  GraphQLScalarType,
  GraphQLList,
  Kind,
  GraphQLError,
} from 'graphql';

import { Duration } from '../Duration';

function parseValue(value: any): Duration {
  if (value instanceof Duration) {
    return value;
  }
  try {
    if (typeof value === 'number'
    || (typeof value === 'string' && /^[0-9]+$/.test(value))) {
      return Duration.fromMillis(
        typeof value === 'string'
          ? parseInt(value, 10)
          : value,
      );
    } else if (typeof value === 'string') {
      return Duration.fromISO(value);
    }
  } catch(error) {
    throw new GraphQLError(error.message);
  }
  throw new GraphQLError('Invalid Duration');
}

export const GraphQLDuration = new GraphQLScalarType({
  name: 'Duration',
  description: 'An ISO-8601 duration string.',
  serialize(value: Duration) {
    return value.toString();
  },
  parseValue,
  parseLiteral(valueNode) {
    if (valueNode.kind === Kind.STRING) {
      return parseValue(valueNode.value);
    } else if (valueNode.kind === Kind.INT) {
      return parseValue(valueNode.value);
    }
    return null;
  },
});
