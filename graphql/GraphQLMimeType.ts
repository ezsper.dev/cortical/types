import {
  GraphQLScalarType,
  Kind,
  GraphQLError,
} from 'graphql';
import * as isMimeType from 'validator/lib/isMimeType';

export const GraphQLMimeType = new GraphQLScalarType({
  name: 'MimeType',
  description: `An RFC 2425 compliant Mime type string.`,
  serialize (value) {
    if (typeof value !== 'string') {
      return null
    }
    if (isMimeType(value)) {
      return value
    }
    return null
  },
  parseValue (value) {
    if (typeof value !== 'string') {
      throw new GraphQLError('', [])
    }
    if (isMimeType(value)) {
      return value
    }
    throw new GraphQLError('', [])
  },
  parseLiteral (ast) {
    if (ast.kind !== Kind.STRING) {
      throw new GraphQLError(`Type should be "String", found ${ast.kind}.`, [ast])
    }
    if (isMimeType(ast.value)) {
      return ast.value
    }
    throw new GraphQLError(`Invalid MimeType literal.\n${ast.value} is not MimeType.`, [ast])
  }
});
