import {
  GraphQLScalarType,
  GraphQLList,
  Kind,
  GraphQLError,
} from 'graphql';
import { DateTimeOptions } from 'luxon';
import { Interval } from '../Interval';
import { parseValue as parseDateTime } from './GraphQLDateTime';

function parseValue(
  value: any,
  dateOptions?: DateTimeOptions,
): Interval {
  const options = {
    zone: 'UTC',
    locale: 'en-US',
    ...dateOptions,
  };
  let interval: Interval | undefined;
  try {
    if (value instanceof Interval) {
      interval = value;
    } else if (Array.isArray(value) && value.length === 2) {
      interval = Interval.fromDateTimes(
        parseDateTime(value[0], options, false),
        parseDateTime(value[1], options, true),
      );
    } else if (typeof value === 'string') {
      interval = Interval.fromISO(value, options);
    }
  } catch(error) {
    throw new GraphQLError(error.message);
  }
  if (interval == null || !interval.isValid) {
    throw new GraphQLError('Invalid Interval');
  }
  return interval;
}

export function createScalar(dateOptions?: DateTimeOptions) {
  const options = {
    zone: 'UTC',
    locale: 'en-US',
    ...dateOptions,
  };
  const { zone, locale } = options;
  return new GraphQLScalarType({
    name: 'Interval',
    description: 'An ISO-8601 interval string.',
    serialize: value => {
      const parsedValue = parseValue(value, options);
      if (parsedValue) {
        return Interval.fromDateTimes(
          parsedValue.start.setZone(zone).setLocale(locale),
          parsedValue.end.setZone(zone).setLocale(locale),
        ).toISO();
      }
      return null;
    },
    parseValue: (value) => parseValue(value, options),
    parseLiteral(valueNode) {
      if (valueNode.kind === Kind.STRING) {
        return parseValue(valueNode.value, options);
      } else if (valueNode.kind === Kind.INT) {
        return parseValue(valueNode.value, options);
      } else if (valueNode.kind === Kind.LIST) {
        return parseValue(
          valueNode.values.map(
            valueNode => 'value' in valueNode ? valueNode.value : null,
          ),
          options,
        );
      }
      return null;
    },
  });
}

export const GraphQLInterval = createScalar();
