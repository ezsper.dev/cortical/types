import {
  GraphQLScalarType,
  Kind,
  GraphQLError,
} from 'graphql';
import * as isMobilePhone from 'validator/lib/isMobilePhone';

export const GraphQLPhone = new GraphQLScalarType({
  name: 'Phone',
  description: `An RFC 3191 and RFC 3192 compliant Phone string`,
  serialize (value) {
    if (typeof value === 'string' && value.charAt(0) !== '+') {
      if (isMobilePhone(value, 'any')) {
        return value
      }
    }
    return null
  },
  parseValue (value) {
    if (typeof value !== 'string') {
      throw new GraphQLError('', [])
    }
    if (isMobilePhone(value, 'any')) {
      return value
    }
    throw new GraphQLError('', [])
  },
  parseLiteral (ast) {
    if (ast.kind !== Kind.STRING) {
      throw new GraphQLError(`Type should be "String", found ${ast.kind}.`, [ast])
    }
    if (ast.value.charAt(0) !== '+') {
      throw new GraphQLError(`Invalid Mobile Phone literal.\nMust start with "+" followed by country code.`, [ast])
    }
    if (isMobilePhone(ast.value, 'any')) {
      return ast.value
    }
    throw new GraphQLError(`Invalid Mobile Phone literal.\n${ast.value} is not a valid Mobile Phone.`, [ast])
  }
});
