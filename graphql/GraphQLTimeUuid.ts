import {
  GraphQLScalarType,
  GraphQLList,
  Kind,
  GraphQLError,
} from 'graphql';

import { TimeUuid } from '../TimeUuid';

function parseValue(value: any): TimeUuid {
  if (value instanceof TimeUuid) {
    return value;
  }
  if (typeof value === 'string') {
    try {
      return TimeUuid.fromString(value);
    } catch(error) {
      throw new GraphQLError(error.message);
    }
  }
  throw new GraphQLError('Invalid uuid');
}

export const GraphQLTimeUuid = new GraphQLScalarType({
  name: 'TimeUuid',
  description: 'A 128-bit number used to uniquely identify some object or entity',
  serialize(value: TimeUuid) {
    return TimeUuid.toString();
  },
  parseValue,
  parseLiteral(valueNode) {
    if (valueNode.kind === Kind.STRING) {
      return parseValue(valueNode.value);
    }
    return null;
  },
});
