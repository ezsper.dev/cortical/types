import {
  GraphQLInterfaceType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLList,
} from 'graphql';

import { GraphQLNode } from './GraphQLNode';
import { ConnectionEdge } from '../Connection';

export const GraphQLConnectionEdge = new GraphQLInterfaceType({
  name: 'ConnectionEdge',
  description: 'The standardized Relay object for dealing with pagination edges on Graph',
  fields: {
    cursor: {
      type: new GraphQLNonNull(GraphQLString),
      description: "The cursor of the edge",
      args: {},
    },
    node: {
      type: GraphQLNode,
      description: "The connection edges",
      args: {},
    },
  },
});
