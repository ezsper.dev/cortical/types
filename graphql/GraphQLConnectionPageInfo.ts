import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLBoolean,
} from 'graphql';

import { GraphQLNode } from './GraphQLNode';

export const GraphQLConnectionPageInfo = new GraphQLObjectType({
  name: 'ConnectionPageInfo',
  description: 'The standardized Relay object for dealing with pagination info on Graph',
  fields: {
    startCursor: {
      type: GraphQLString,
      description: "The start cursor of the connection",
      args: {},
    },
    hasPreviousPage: {
      type: GraphQLBoolean,
      description: "If has a previous page in the connection position",
      args: {},
    },
    hasNextPage: {
      type: GraphQLBoolean,
      description: "If has a next page in the connection position",
      args: {},
    },
    endCursor: {
      type: GraphQLString,
      description: "The end cursor of the connection",
      args: {},
    },
  },
});
