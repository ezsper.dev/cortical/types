import {
  GraphQLScalarType,
  GraphQLList,
  Kind
} from 'graphql';

import {
  isString,
  isInteger,
} from 'lodash';

import { Long } from '../Long';

function parseLong(value: any) {
  if (value instanceof Long) {
    return value;
  }
  if(isString(value)) {
    const matches = value.match(/^\s*([0-9]+)\s*$/);
    if(matches) {
      return Long.fromString(matches[0]);
    }
  }
  return Long.fromInt(value);
}

export const GraphQLLong = new GraphQLScalarType({
  name: 'Long',
  description: 'A 64-bit signed integer, known as Long',
  serialize: value => {
    const parsedValue = parseLong(value);
    const str1 = parsedValue.toString();
    const str2: any = "" + parsedValue.toNumber();
    if(str1 !== str2) {
      return str1;
    }
    return str2 - 0;
  },
  parseValue: parseLong,
  parseLiteral(valueNode) {
    if (valueNode.kind === Kind.STRING || valueNode.kind === Kind.INT) {
      return parseLong(valueNode.value);
    }
    return null;
  }
});
