import {
  GraphQLInterfaceType,
  GraphQLNonNull,
  GraphQLID,
} from 'graphql';

export const GraphQLNode = new GraphQLInterfaceType({
  name: 'Node',
  description: 'The stardized global type fetching',
  fields: {
    id: {
      type: new GraphQLNonNull(GraphQLID),
      description: "The node's global ID",
      args: {},
    }
  },
});
