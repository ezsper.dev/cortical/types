import {
  GraphQLScalarType,
  Kind,
  GraphQLError,
} from 'graphql';
import { isFQDN } from 'validator';

export const GraphQLFQDN = new GraphQLScalarType({
  name: 'FQDN',
  description: `An RFC 4703 compliant FQDN string.'`,
  serialize (value) {
    if (typeof value !== 'string') {
      return null
    }
    if (isFQDN(value)) {
      return value
    }
    return null
  },
  parseValue (value) {
    if (typeof value !== 'string') {
      throw new GraphQLError('', [])
    }
    if (isFQDN(value)) {
      return value
    }
    throw new GraphQLError('', [])
  },
  parseLiteral (ast) {
    if (ast.kind !== Kind.STRING) {
      throw new GraphQLError(`Type should be "String", found ${ast.kind}.`, [ast])
    }
    if (isFQDN(ast.value)) {
      return ast.value
    }
    throw new GraphQLError(`Invalid FQDN literal.\n${ast.value} is not FQDN.`, [ast])
  }
});
