import {
  GraphQLScalarType,
  GraphQLList,
  Kind,
  GraphQLError,
} from 'graphql';
import { DateTimeOptions } from 'luxon';
import { DateTime } from '../DateTime';

export function parseValue(
  value: any,
  dateOptions?: DateTimeOptions,
  end = false,
): DateTime {
  const options = {
    zone: 'UTC',
    locale: 'en-US',
    ...dateOptions,
  };
  let date: DateTime | undefined;
  try {
    if (value instanceof DateTime) {
      date = value;
    } else if (value instanceof Date) {
      date = DateTime.fromJSDate(value, options);
    } else if (typeof value === 'string') {
      const isoString = !/(T| )([0-9.:]+)/.test(value)
        ? value.replace(/^([0-9-]+)/, end ? '$1T23:59:59' : '$1T00:00:00')
        : value;
      date = DateTime.fromISO(isoString, options);
    } else if (typeof value === 'number') {
      date = DateTime.fromMillis(value, options);
    }
  } catch(error) {
    throw new GraphQLError(error.message);
  }
  if (date == null || !date.isValid) {
    throw new GraphQLError('Invalid DateTime');
  }
  return date;
}

export function createScalar(dateOptions?: DateTimeOptions) {
  const options = {
    zone: 'UTC',
    locale: 'en-US',
    ...dateOptions,
  };
  const { zone, locale } = options;
  return new GraphQLScalarType({
    name: 'DateTime',
    description: 'An ISO-8601 encoded UTC date string (without time component)',
    serialize: value => {
      const parsedValue = parseValue(value);
      if (parsedValue) {
        return parsedValue.setZone(zone).setLocale(locale).toISO();
      }
      return null;
    },
    parseValue: (value) => parseValue(value, options),
    parseLiteral(valueNode) {
      if (valueNode.kind === Kind.STRING) {
        return parseValue(valueNode.value, options);
      } else if (valueNode.kind === Kind.INT) {
        return parseValue(valueNode.value, options);
      }
      return null;
    }
  });
}

export const GraphQLDateTime = createScalar();
