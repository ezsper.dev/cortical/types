import {
  GraphQLScalarType,
  GraphQLList,
  Kind,
  GraphQLError,
} from 'graphql';

import { DateTimeOptions } from 'luxon';
import { Interval } from '../Interval';
import { parseValue as parseLocalDate } from './GraphQLLocalDate';

function parseValue(
  value: any,
  dateOptions?: DateTimeOptions,
): Interval {
  const options = {
    zone: 'UTC',
    locale: 'en-US',
    ...dateOptions,
  };
  let interval: Interval | undefined;
  try {
    if (value instanceof Interval) {
      interval = value;
    } else if (Array.isArray(value) && value.length === 2) {
      interval = Interval.fromDateTimes(
        parseLocalDate(value[0], options, false),
        parseLocalDate(value[1], options, true),
      );
    } else if (typeof value === 'string') {
      interval = Interval.fromISO(value, options);
    }
  } catch(error) {
    throw new GraphQLError(error.message);
  }
  if (interval == null || !interval.isValid) {
    throw new GraphQLError('Invalid Interval');
  }
  return interval;
}

export function createScalar(dateOptions?: DateTimeOptions) {
  const options = {
    zone: 'UTC',
    locale: 'en-US',
    ...dateOptions,
  };
  const { zone, locale } = options;
  return new GraphQLScalarType({
    name: 'LocalDateInterval',
    description: 'An ISO-8601 interval string.',
    serialize: value => {
      let interval: Interval;
      if (typeof value === 'string') {
        interval = parseValue(value, options);
      } else {
        interval = value;
      }
      if (interval) {
        return [
          interval.start.setZone(zone).setLocale(locale).toISO().replace(/(T| )([0-9.:]+)/g, ''),
          interval.end.setZone(zone).setLocale(locale).toISO().replace(/(T| )([0-9.:]+)/g, ''),
        ];
      }
      return null;
    },
    parseValue: (value) => parseValue(value, options),
    parseLiteral(valueNode) {
      if (valueNode.kind === Kind.STRING) {
        return parseValue(valueNode.value, options);
      } else if (valueNode.kind === Kind.INT) {
        return parseValue(valueNode.value, options);
      } else if (valueNode.kind === Kind.LIST) {
        return parseValue(
          valueNode.values.map(
            valueNode => 'value' in valueNode ? valueNode.value : null,
          ),
          options,
        );
      }
      return null;
    },
  });
}

export const GraphQLLocalDateInterval = createScalar();
