import {
  GraphQLScalarType,
  Kind,
  GraphQLError,
} from 'graphql';
import { DateTime } from 'luxon';

const timezones: string[] = Object.keys(require('countries-and-timezones').getAllTimezones());
const lowerCasedTimezones = timezones.map(timezone => timezone.toLowerCase());

const resolveTimeZone = (value: string): string | null => {
  const zoneIndex = lowerCasedTimezones.indexOf(value.toLowerCase());
  if (zoneIndex >= 0) {
    const zone = DateTime.local().setZone(timezones[zoneIndex]);
    if (zone.isValid) {
      return zone.zoneName;
    }
  }
  return null;
};

export const GraphQLTimeZone = new GraphQLScalarType({
  name: 'TimeZone',
  description: `An IANA-specified zone.`,
  serialize (value) {
    if (typeof value !== 'string') {
      return null;
    }
    return resolveTimeZone(value);
  },
  parseValue (value) {
    const timeZone = resolveTimeZone(value);
    if (timeZone != null) {
      return timeZone;
    }
    throw new GraphQLError('', [])
  },
  parseLiteral (ast) {
    if (ast.kind !== Kind.STRING) {
      throw new GraphQLError(`Type should be "String", found ${ast.kind}.`, [ast])
    }
    const timeZone = resolveTimeZone(ast.value);
    if (timeZone != null) {
      return timeZone;
    }
    throw new GraphQLError(`Invalid TimeZone literal.\n${ast.value} is not a valid IANA-specified zone.`, [ast])
  }
});
