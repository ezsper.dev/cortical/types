import { ID } from './ID';

export interface Node {
  id: ID;
}
