"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const builtinTypes = require("./graphql");
const externalTypes = require('@cortical/core/hooks/externalTypes').default;
externalTypes.addFilter('types', (types) => {
    return [
        ...types,
        ...Object.values(builtinTypes)
            .filter(value => (value instanceof graphql_1.GraphQLScalarType
            || value instanceof graphql_1.GraphQLInterfaceType)),
    ];
});
//# sourceMappingURL=bootstrap.js.map