/// <reference types="node" />
export declare class Uuid {
    private buffer;
    static fromString(value: string): Uuid;
    static random(): Uuid;
    constructor(buffer: Buffer);
    getBuffer(): Buffer;
    equals(uuid: Uuid): boolean;
    toString(): string;
    toJSON(): string;
}
