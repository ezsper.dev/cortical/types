import { GraphQLResolveInfo, GraphQLObjectType } from 'graphql';
declare global {
    interface ObjectTypeStatic<F extends {}> {
        graphqlType: GraphQLObjectType;
        forge(data: F): F & ObjectTypeMember<F>;
        isTypeOf(value: any, context: any, info: GraphQLResolveInfo): boolean;
        new (data: F): F & ObjectTypeMember<F>;
    }
    interface ObjectTypeMember<F extends {}> {
        set<K extends keyof F>(key: K, value: F[K]): this;
        set(data: Partial<F>): this;
        get<K extends keyof F>(key: K): F[K];
        toPlainObject(): Partial<F>;
        toJSON(): Partial<F>;
        toGraph(context: any, info: GraphQLResolveInfo): Partial<F> | Promise<Partial<F>>;
    }
}
export { ObjectTypeStatic, ObjectTypeMember };
export declare function ObjectType<F>(Fields: {
    new (): F;
}): ObjectTypeStatic<F>;
