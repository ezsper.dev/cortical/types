"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function lowerFirst(value) {
    return value.charAt(0).toLowerCase() + value.slice(1);
}
exports.ObjectType = (function () {
    // tslint:disable-next-line function-name
    function ObjectType(Fields) {
        class AbstractObjectType extends Fields {
            constructor(data) {
                super();
                this.set(data);
                const descriptions = {};
                let proto = this;
                while (proto) {
                    if (proto === Object.prototype) {
                        break;
                    }
                    Object.assign(descriptions, Object.getOwnPropertyDescriptors(proto));
                    proto = Object.getPrototypeOf(proto);
                }
                for (const key in descriptions) {
                    const match = key.match(/^resolve([A-Z].+)$/);
                    if (match == null) {
                        continue;
                    }
                    if (typeof descriptions[key].value !== 'function') {
                        continue;
                    }
                    const defaultResolver = this[key];
                    Object.defineProperty(this, key, {
                        value: function resolver(...args) {
                            const value = defaultResolver.apply(this, args);
                            const resolve = (val) => {
                                this[lowerFirst(match[1])] = val;
                                return val;
                            };
                            if (value instanceof Promise) {
                                return value.then(resolve);
                            }
                            return resolve(value);
                        },
                    });
                }
            }
            static forge(data) {
                return new this(data);
            }
            static isTypeOf(value, context, info) {
                if (value instanceof this) {
                    return true;
                }
                if (value.__typename === this.name) {
                    return true;
                }
                return false;
            }
            set(...args) {
                if (typeof this.data === 'undefined') {
                    this.data = {};
                }
                let data;
                if (typeof args[0] === 'string') {
                    const [key, value] = args;
                    data = { [key]: value };
                }
                else {
                    data = args[0];
                }
                for (const key in data) {
                    this[key] = data[key];
                    Object.defineProperty(this, key, {
                        get() {
                            return this.data[key];
                        },
                        set(value) {
                            return this.data[key] = value;
                        }
                    });
                }
                Object.assign(this.data, data);
                return this;
            }
            get(key) {
                return this.data[key];
            }
            toPlainObject() {
                const data = {};
                for (const key in this) {
                    if (key === 'data') {
                        continue;
                    }
                    data[key] = this[key];
                }
                const descriptions = {};
                let proto = this.constructor.prototype;
                while (proto) {
                    if (proto === Object.prototype) {
                        break;
                    }
                    Object.assign(descriptions, Object.getOwnPropertyDescriptors(proto));
                    proto = Object.getPrototypeOf(proto);
                }
                for (const key in descriptions) {
                    if (typeof descriptions[key].get !== 'undefined') {
                        data[key] = this[key];
                    }
                }
                for (const key in this.data) {
                    if (typeof this.data[key] !== 'undefined') {
                        data[key] = this.data[key];
                    }
                }
                return data;
            }
            toJSON() {
                return this.toPlainObject();
            }
            toGraph(context, info) {
                return this.toPlainObject();
            }
        }
        AbstractObjectType.Fields = Fields;
        Object.defineProperty(AbstractObjectType, 'name', {
            configurable: true,
            value: `ObjectType<${Fields.name}>`
        });
        return AbstractObjectType;
    }
    ObjectType.extend = function extendObjectType(ObjectType, Fields) {
        var _a;
        if (Fields != null) {
            if (!(Fields === ObjectType.Fields || Fields.prototype instanceof ObjectType.Fields)) {
                throw new Error(`Fields "${Fields.name}" must extends "${ObjectType.name}.Fields"`);
            }
        }
        return _a = class extends ObjectType {
            },
            _a.Fields = Fields != null ? Fields : ObjectType.Fields,
            _a;
    };
    return ObjectType;
})();
//# sourceMappingURL=ObjectType.js.map