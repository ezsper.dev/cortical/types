# Cortical Types

A set of TypeScript types supported on GraphQL

## ROADMAP

### Generic

* [ ] InetAddress
* [x] URI
* [x] Slug
* [x] Uuid
* [x] Email
* [x] Phone
* [x] FQDN
* [x] Country
* [x] Language

### Special

* [x] Any
* [ ] Object

### Number

* [x] Integer
* [x] Long
* [ ] BigInt
* [ ] BigDecimal


### Date/Time

* [x] Duration
* [x] Interval
* [x] TimeUuid
* [x] DateTime
* [x] LocalDate
* [x] LocalDateInterval
* [x] TimeZone
* [ ] LocalTime


### Geometry

* [ ] LineString
* [ ] Polygon
* [ ] Point
* [ ] Distance


### Relay

* [x] Node
* [x] Connection (with utilities)

#### Utilities

* [x] connectionFromArray
* [x] GlobalId


## Usage TypeScript type

```
import { Long } from '@cortical/types';
```

## Usage GraphQL

```
import { GraphQLLong } from '@cortical/types/graphql';
```
