import { Node } from './Node';
import { Integer } from './Integer';
import { GlobalId, KeySignature } from './GlobalId';
/**
 * Implement your method arguments with this interface
 * to make it clear that the field returns connection type.
 */
export interface ConnectionArguments {
    first?: Integer;
    after?: string;
    last?: Integer;
    before?: string;
}
export declare type ConnectionEdgeFields = 'cursor' | 'node';
export interface ConnectionPageInfo {
    startCursor?: string;
    endCursor?: string;
    hasNextPage?: boolean;
    hasPreviousPage?: boolean;
}
export interface ConnectionEdge<T extends Node = Node> {
    cursor: string;
    node: T;
}
export declare type ConnectionFields = 'edges' | 'pageInfo';
export interface Connection<T extends Node = Node, E extends ConnectionEdge<T> = ConnectionEdge<T>, P extends ConnectionPageInfo = ConnectionPageInfo> {
    edges: E[];
    nodes: T[];
    pageInfo: P;
}
export interface ConnectionArraySliceMetaInfo {
    sliceStart: number;
    arrayLength: number;
}
export declare abstract class CursorId<S extends KeySignature> extends GlobalId<S> {
    static entityName: string;
}
export declare function connectionFromArraySlice<T extends Node>(nodes: T[], args: ConnectionArguments, meta: ConnectionArraySliceMetaInfo): Connection<T>;
/**
 * A simple function that accepts an array and connection arguments, and returns
 * a connection object for use in GraphQL. It uses array offsets as pagination,
 * so pagination will only work if the array is static.
 */
export declare function connectionFromArray<T extends Node>(nodes: T[], args: ConnectionArguments): Connection<T>;
export interface ConnectionParsedArgsWithoutPosition {
    order: 'first' | 'last';
    limit: Integer;
    pos: undefined;
    from: undefined;
}
export interface ConnectionParsedArgsWithPosition {
    order: 'first' | 'last';
    limit: Integer;
    pos: 'before' | 'after';
    from: string;
}
export declare type ConnectionParsedArgs = ConnectionParsedArgsWithoutPosition | ConnectionParsedArgsWithPosition;
export interface ParseConnectionArgsOptions {
    requiredLimit?: boolean;
    defaultLimit?: Integer;
    minLimit?: Integer;
    maxLimit?: Integer;
    defaultOrder?: 'first' | 'last';
}
export declare function parseConnectionArgs(args: ConnectionArguments, options?: ParseConnectionArgsOptions): ConnectionParsedArgs;
/**
 * A simple function that accepts an array and connection arguments, and returns
 * a connection object for use in GraphQL. It uses array offsets as pagination,
 * so pagination will only work if the array is static.
 */
export declare function connection<T extends Node>(nodes: T[], mapEdge: (node: T, index: number) => ConnectionEdge<T>, pageInfo: ConnectionPageInfo): Connection<T>;
