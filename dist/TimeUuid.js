"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const crypto = require("crypto");
const Uuid_1 = require("./Uuid");
const Long_1 = require("./Long");
/**
 * Oct 15, 1582 in milliseconds since unix epoch
 * @private
 */
const _unixToGregorian = 12219292800000;
/**
 * 10,000 ticks in a millisecond
 */
const _ticksInMs = 10000;
/**
 * Counter used to generate up to 10000 different timeuuid values with the same Date
 * @private
 */
let _ticks = 0;
/**
 * Counter used to generate ticks for the current time
 * @private
 */
let _ticksForCurrentTime = 0;
/**
 * Remember the last time when a ticks for the current time so that it can be reset
 * @private
 */
let _lastTimestamp = 0;
/**
 * Returns a buffer of length 2 representing the clock identifier
 * @private
 */
function getClockId(clockId) {
    let buffer;
    if (typeof clockId === 'string') {
        buffer = new Buffer(clockId, 'ascii');
    }
    else if (clockId instanceof Buffer) {
        buffer = clockId;
    }
    if (typeof buffer === 'undefined') {
        buffer = crypto.randomBytes(2);
    }
    else if (buffer.length !== 2) {
        throw new TypeError('Clock identifier must have 2 bytes');
    }
    return buffer;
}
/**
 * Returns a buffer of length 6 representing the clock identifier
 * @private
 */
function getNodeId(nodeId) {
    let buffer;
    if (typeof nodeId === 'string') {
        buffer = new Buffer(nodeId, 'ascii');
    }
    else if (nodeId instanceof Buffer) {
        buffer = nodeId;
    }
    if (typeof buffer === 'undefined') {
        //Generate
        buffer = crypto.randomBytes(6);
    }
    else if (buffer.length !== 6) {
        throw new Error('Node identifier must have 6 bytes');
    }
    return buffer;
}
/**
 * Returns the ticks portion of a timestamp.  If the ticks are not provided an internal counter is used that gets reset at 10000.
 * @private
 */
function getTicks(ticks) {
    if (typeof ticks !== 'undefined' && ticks >= _ticksInMs) {
        _ticks++;
        if (_ticks >= _ticksInMs) {
            _ticks = 0;
        }
        ticks = _ticks;
    }
    return ticks;
}
/**
 * Returns an object with the time representation of the date expressed in milliseconds since unix epoch
 * and a ticks property for the 100-nanoseconds precision.
 * @private
 */
function getTimeWithTicks(date, ticks = _ticksForCurrentTime) {
    if (typeof date === 'undefined' || isNaN(date.getTime())) {
        // time with ticks for the current time
        date = new Date();
        const time = date.getTime();
        _ticksForCurrentTime++;
        if (_ticksForCurrentTime > _ticksInMs || time > _lastTimestamp) {
            _ticksForCurrentTime = 0;
            _lastTimestamp = time;
        }
    }
    return {
        time: date.getTime(),
        ticks: getTicks(ticks)
    };
}
function writeTime(buffer, time, ticks) {
    //value time expressed in ticks precision
    const value = Long_1.Long
        .fromNumber(time + _unixToGregorian)
        .multiply(Long_1.Long.fromNumber(10000))
        .add(Long_1.Long.fromNumber(ticks));
    const timeHigh = value.getHighBitsUnsigned();
    buffer.writeUInt32BE(value.getLowBitsUnsigned(), 0);
    buffer.writeUInt16BE(timeHigh & 0xffff, 4);
    buffer.writeUInt16BE(timeHigh >>> 16 & 0xffff, 6);
}
class TimeUuid extends Uuid_1.Uuid {
    constructor(value, ticks, nodeId, clockId) {
        let buffer;
        if (value instanceof Buffer) {
            if (value.length !== 16) {
                throw new TypeError('Buffer for v1 uuid not valid');
            }
            buffer = value;
        }
        else {
            const timeWithTicks = getTimeWithTicks(value, ticks);
            nodeId = getNodeId(nodeId);
            clockId = getClockId(clockId);
            buffer = Buffer.allocUnsafe(16);
            // Positions 0-7 Timestamp
            writeTime(buffer, timeWithTicks.time, timeWithTicks.ticks);
            // Position 8-9 Clock
            clockId.copy(buffer, 8, 0);
            // Positions 10-15 Node
            nodeId.copy(buffer, 10, 0);
            // Version Byte: Time based
            // 0001xxxx
            // turn off first 4 bits
            buffer[6] = buffer[6] & 0x0f;
            // turn on fifth bit
            buffer[6] = buffer[6] | 0x10;
            // IETF Variant Byte: 1.0.x
            // 10xxxxxx
            // turn off first 2 bits
            buffer[8] = buffer[8] & 0x3f;
            // turn on first bit
            buffer[8] = buffer[8] | 0x80;
        }
        super(buffer);
    }
}
exports.TimeUuid = TimeUuid;
//# sourceMappingURL=TimeUuid.js.map