"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const GlobalId_1 = require("./GlobalId");
class CursorId extends GlobalId_1.GlobalId {
}
CursorId.entityName = 'cursor';
exports.CursorId = CursorId;
const cursorType = 'ArrayConnection';
/**
 * Creates the cursor string from an offset.
 */
function offsetToCursor(offset) {
    return CursorId.stringify(cursorType, 'Integer', offset);
}
/**
 * Rederives the offset from the cursor string.
 */
function cursorToOffset(cursor) {
    return CursorId.parse(cursor, 'Integer', cursorType);
}
/**
 * Given an optional cursor and a default offset, returns the offset
 * to use; if the cursor contains a valid offset, that will be used,
 * otherwise it will be the default.
 */
function getOffsetWithDefault(cursor, defaultOffset) {
    if (typeof cursor !== 'string') {
        return defaultOffset;
    }
    const offset = cursorToOffset(cursor);
    return isNaN(offset) ? defaultOffset : offset;
}
function connectionFromArraySlice(nodes, args, meta) {
    const { after, before, first, last } = args;
    const { sliceStart, arrayLength } = meta;
    const sliceEnd = sliceStart + nodes.length;
    const beforeOffset = getOffsetWithDefault(before, arrayLength);
    const afterOffset = getOffsetWithDefault(after, -1);
    let startOffset = Math.max(sliceStart - 1, afterOffset, -1) + 1;
    let endOffset = Math.min(sliceEnd, beforeOffset, arrayLength);
    if (typeof first === 'number') {
        if (first < 0) {
            throw new graphql_1.GraphQLError('Argument "first" must be a non-negative integer');
        }
        endOffset = Math.min(endOffset, startOffset + first);
    }
    if (typeof last === 'number') {
        if (last < 0) {
            throw new graphql_1.GraphQLError('Argument "last" must be a non-negative integer');
        }
        startOffset = Math.max(startOffset, endOffset - last);
    }
    // If supplied slice is too large, trim it down before mapping over it.
    const slice = nodes.slice(Math.max(startOffset - sliceStart, 0), nodes.length - (sliceEnd - endOffset));
    const lowerBound = after ? (afterOffset + 1) : 0;
    const upperBound = before ? beforeOffset : arrayLength;
    const pageInfo = {
        hasPreviousPage: typeof last === 'number' ? startOffset > lowerBound : false,
        hasNextPage: typeof first === 'number' ? endOffset < upperBound : false,
    };
    const mapEdge = (node, index) => ({
        node,
        cursor: offsetToCursor(index),
    });
    return connection(slice, mapEdge, pageInfo);
}
exports.connectionFromArraySlice = connectionFromArraySlice;
/**
 * A simple function that accepts an array and connection arguments, and returns
 * a connection object for use in GraphQL. It uses array offsets as pagination,
 * so pagination will only work if the array is static.
 */
function connectionFromArray(nodes, args) {
    return connectionFromArraySlice(nodes, args, {
        sliceStart: 0,
        arrayLength: nodes.length,
    });
}
exports.connectionFromArray = connectionFromArray;
function parseConnectionArgs(args, options = {}) {
    let order = typeof options.defaultOrder !== 'undefined'
        ? options.defaultOrder
        : 'first';
    let pos;
    let limit;
    let from;
    if (typeof args.first !== 'undefined' && typeof args.last !== 'undefined') {
        throw new graphql_1.GraphQLError(`You must choose "first" or "last"`);
    }
    if (typeof args.first === 'number') {
        if (args.first < 1) {
            throw new graphql_1.GraphQLError(`Argument "first" must be greater than 0`);
        }
        order = 'first';
        limit = args.first;
    }
    else if (typeof args.last === 'number') {
        order = 'last';
        limit = args.last;
        if (args.last < 1) {
            throw new graphql_1.GraphQLError(`Argument "last" must be greater than 0`);
        }
    }
    if (typeof limit === 'undefined') {
        limit = options.defaultLimit;
    }
    if (limit == null) {
        if (options.requiredLimit === true) {
            throw new graphql_1.GraphQLError(`You must specify "first" or "last" arguments`);
        }
        limit = 0;
    }
    if (typeof options.minLimit !== 'undefined' && limit < options.minLimit) {
        throw new graphql_1.GraphQLError(`The "${order}" argument must be greater or equal "${options.minLimit}"`);
    }
    else if (typeof options.maxLimit !== 'undefined' && limit > options.maxLimit) {
        throw new graphql_1.GraphQLError(`The "${order}" argument must be greater or equal "${options.maxLimit}"`);
    }
    if (typeof args.before === 'string') {
        pos = 'before';
        from = args.before;
    }
    else if (typeof args.after === 'string') {
        pos = 'after';
        from = args.after;
    }
    else {
        return {
            order,
            limit,
            pos: undefined,
            from: undefined,
        };
    }
    return { pos, order, limit, from };
}
exports.parseConnectionArgs = parseConnectionArgs;
/**
 * A simple function that accepts an array and connection arguments, and returns
 * a connection object for use in GraphQL. It uses array offsets as pagination,
 * so pagination will only work if the array is static.
 */
function connection(nodes, mapEdge, pageInfo) {
    const edges = nodes.map((node, index) => mapEdge(node, index));
    return {
        nodes,
        edges,
        pageInfo: Object.assign({}, (edges.length > 0
            ? {
                startCursor: edges[0].cursor,
                endCursor: edges[edges.length - 1].cursor,
            }
            : null), pageInfo),
    };
}
exports.connection = connection;
//# sourceMappingURL=Connection.js.map