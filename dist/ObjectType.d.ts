import { GraphQLResolveInfo, GraphQLObjectType } from 'graphql';
declare global {
    interface ObjectTypeStatic<F extends {}> {
        graphqlType: GraphQLObjectType;
        Fields: {
            new (): F;
        };
        forge(data: F): F & ObjectTypeMember<F>;
        isTypeOf(value: any, context: any, info: GraphQLResolveInfo): boolean;
        new (data: F): F & ObjectTypeMember<F>;
    }
    type ObjectTypeStaticExtended<C extends ObjectTypeStatic<{}>, F extends {
        [K in keyof InstanceType<C['Fields']>]: any;
    }> = {
        [K in keyof C]: C[K];
    } & {
        graphqlType: GraphQLObjectType;
        Fields: {
            new (): F;
        };
        forge(data: F): InstanceType<C> & F & ObjectTypeMember<F>;
        isTypeOf(value: any, context: any, info: GraphQLResolveInfo): boolean;
        new (data: F): InstanceType<C> & F & ObjectTypeMember<F>;
    };
    interface ObjectTypeMember<F extends {}> {
        set<K extends keyof F>(key: K, value: F[K]): this;
        set(data: Partial<F>): this;
        get<K extends keyof F>(key: K): F[K];
        toPlainObject(): Partial<F>;
        toJSON(): Partial<F>;
        toGraph(context: any, info: GraphQLResolveInfo): Partial<F> | Promise<Partial<F>>;
    }
}
export { ObjectTypeStatic, ObjectTypeMember };
export declare const ObjectType: {
    <F>(Fields: {
        new (): F;
    }): ObjectTypeStatic<F>;
    extend<C extends ObjectTypeStatic<{}>, F extends {
        [K in keyof InstanceType<C['Fields']>]: any;
    } = InstanceType<C['Fields']>>(ObjectType: C, Fields?: {
        new (): F;
    }): ObjectTypeStaticExtended<C, F>;
};
