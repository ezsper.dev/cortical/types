/// <reference types="node" />
import { ID } from './ID';
import { Long } from './Long';
import { Uuid } from './Uuid';
import { TimeUuid } from './TimeUuid';
import { URI } from './URI';
import { Email } from './Email';
import { Phone } from './Phone';
import { DateTime } from './DateTime';
export interface GlobalIdCastRequired {
    'Uuid': Uuid;
    'TimeUuid': TimeUuid;
    'DateTime': DateTime;
    'LocalDate': DateTime;
    'Long': Long;
    'String': string;
    'Integer': number;
    'Email': Email;
    'URI': URI;
    'Phone': Phone;
    'Float': number;
    'Blob': Buffer;
}
export interface GlobalIdCastOptional {
    'Uuid?': Uuid | void;
    'TimeUuid?': TimeUuid | void;
    'DateTime?': DateTime | void;
    'LocalDate?': DateTime | void;
    'Long?': Long | void;
    'String?': string | void;
    'Integer?': number | void;
    'Email?': Email | void;
    'URI?': URI | void;
    'Phone?': Phone | void;
    'Float?': number | void;
    'Blob?': Buffer;
}
export interface GlobalIdCast extends GlobalIdCastRequired, GlobalIdCastOptional {
}
export declare type IdValue = Uuid | TimeUuid | DateTime | Long | string | number | Buffer;
export declare type DataId = {
    [key: string]: IdValue | void;
};
export declare type GlobalIdValue<T extends GlobalIdCast[keyof GlobalIdCast] | Buffer> = {
    type: string;
    id: T;
};
export declare type KeySignature = {
    [key: string]: keyof GlobalIdCast;
};
export declare type Key = keyof GlobalIdCastRequired | KeySignature;
/**
 * Helper library to manipulate Relay's global id
 */
export declare class GlobalId<S extends {
    [key: string]: keyof GlobalIdCast;
}> {
    entityName: string;
    signature: S;
    static entityName: string;
    /**
     * Signs a GlobalId entity
     */
    static sign<S extends KeySignature>(entityName: string, signature: S): GlobalId<S>;
    static isValid(value: string, encoding?: string): boolean;
    static validate(id: string, entityName?: string, encoding?: string): boolean;
    static stringify<D extends {
        [key: string]: keyof GlobalIdCast;
    }>(type: string, Key: D, id: {
        [K in keyof D]: GlobalIdCast[D[K]];
    }, encoding?: string): ID;
    static stringify<K extends keyof GlobalIdCastRequired>(type: string, Key: K, id: GlobalIdCastRequired[K], encoding?: string): ID;
    static stringifyValues<D extends {
        [key: string]: keyof GlobalIdCast;
    }>(type: string, Key: D, id: {
        [K in keyof D]: GlobalIdCast[D[K]];
    }, encoding?: string): string;
    static stringifyValues<K extends keyof GlobalIdCastRequired>(type: string, Key: K, id: GlobalIdCastRequired[K], encoding?: string): string;
    static getType(globalId: string, encoding?: string): string;
    static parse<K extends keyof GlobalIdCast>(globalId: ID, cast: K, matchType?: string | string[], encoding?: string): GlobalIdCast[K];
    static parse<D extends {
        [key: string]: keyof GlobalIdCast;
    }>(globalId: ID, cast: D, matchType?: string, encoding?: string): {
        [K in keyof D]: GlobalIdCast[D[K]];
    };
    static parseValues<K extends keyof GlobalIdCast>(strValues: string, cast: K, matchType?: string, encoding?: string): GlobalIdCast[K];
    static parseValues<D extends {
        [key: string]: keyof GlobalIdCast;
    }>(strValues: string, cast: D, matchType?: string, encoding?: string): {
        [K in keyof D]: GlobalIdCast[D[K]];
    };
    constructor(entityName: string, signature: S);
    stringify(data: {
        [K in keyof S]: GlobalIdCast[S[K]];
    }, encoding?: string): ID;
    stringifyValues(data: {
        [K in keyof S]: GlobalIdCast[S[K]];
    }, encoding?: string): string;
    parse(id: ID, encoding?: string): {
        [K in keyof S]: GlobalIdCast[S[K]];
    };
    parseValues(strValues: string, encoding?: string): {
        [K in keyof S]: GlobalIdCast[S[K]];
    };
}
