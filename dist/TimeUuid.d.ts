/// <reference types="node" />
import { Uuid } from './Uuid';
export declare class TimeUuid extends Uuid {
    constructor(value: Buffer | Date, ticks?: number, nodeId?: Buffer | string, clockId?: Buffer | string);
}
