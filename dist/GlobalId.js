"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Long_1 = require("./Long");
const Uuid_1 = require("./Uuid");
const TimeUuid_1 = require("./TimeUuid");
const DateTime_1 = require("./DateTime");
const InvalidIDError_1 = require("./errors/InvalidIDError");
const ESCAPE = (new Buffer('\\'))[0];
const SEPARATOR = (new Buffer(';'))[0];
/**
 * Helper library to manipulate Relay's global id
 */
class GlobalId {
    constructor(entityName, signature) {
        this.entityName = entityName;
        this.signature = signature;
    }
    /**
     * Signs a GlobalId entity
     */
    static sign(entityName, signature) {
        return new GlobalId(entityName, signature);
    }
    static isValid(value, encoding = 'base64') {
        const buffer = new Buffer(value, encoding);
        if (buffer.length > 5) {
            const typeSize = parseInt(buffer.slice(0, 3).toString(), 10);
            if (buffer.slice(0, 3).toString() === `00${typeSize}`.slice(-3)
                && buffer.length > (typeSize + 4)) {
                return true;
            }
        }
        return false;
    }
    static validate(id, entityName = this.entityName, encoding = 'base64') {
        if (!GlobalId.isValid(id, encoding)) {
            throw new InvalidIDError_1.InvalidIDError({
                id,
                encoding,
                matchType: [entityName],
                method: 'parse',
            });
        }
        return true;
    }
    static stringify(type, Key, id, encoding = 'base64') {
        const typeBuf = new Buffer(type);
        if (typeBuf.length > 999) {
            throw new InvalidIDError_1.InvalidIDError({
                encoding,
                message: `The type argument encodes more than 999 bytes`,
                matchType: [type],
                data: id,
                method: 'stringify',
            });
        }
        if (typeBuf.length === 0) {
            throw new InvalidIDError_1.InvalidIDError({
                encoding,
                message: `The type argument encodes 0 bytes`,
                matchType: [type],
                data: id,
                method: 'stringify',
            });
        }
        const stringify = (value) => {
            let buffer;
            if (typeof value === 'undefined') {
                buffer = new Buffer(0);
            }
            else if (value instanceof Uuid_1.Uuid) {
                buffer = value.getBuffer();
            }
            else if (value instanceof Long_1.Long || typeof value === 'number') {
                buffer = new Buffer(value.toString());
            }
            else if (typeof value === 'string') {
                buffer = new Buffer(value);
            }
            else if (value instanceof Buffer) {
                buffer = value;
            }
            else if (value instanceof DateTime_1.DateTime) {
                const iso = value.toISO();
                if (iso == null) {
                    throw new InvalidIDError_1.InvalidIDError({
                        encoding,
                        message: `DateTime is not valid`,
                        matchType: [type],
                        data: id,
                        method: 'stringify',
                    });
                }
                buffer = new Buffer(iso);
            }
            else {
                throw new InvalidIDError_1.InvalidIDError({
                    encoding,
                    message: `Couldn't stringify`,
                    matchType: [type],
                    data: id,
                    method: 'stringify',
                });
            }
            if (typeof value !== 'undefined' && buffer.length === 0) {
                throw new InvalidIDError_1.InvalidIDError({
                    encoding,
                    message: `Data encodes 0 bytes`,
                    matchType: [type],
                    data: id,
                    method: 'stringify',
                });
            }
            let cleanedBuffer = new Buffer(0);
            let offset = 0;
            for (const index of buffer.keys()) {
                if (buffer[index] === SEPARATOR && buffer[index - 1] !== ESCAPE) {
                    cleanedBuffer = Buffer.concat([
                        cleanedBuffer,
                        buffer.slice(offset, index),
                        new Buffer([ESCAPE]),
                        new Buffer([SEPARATOR]),
                    ]);
                    offset = index + 1;
                }
            }
            if (offset < buffer.length) {
                cleanedBuffer = Buffer.concat([
                    cleanedBuffer,
                    buffer.slice(offset, buffer.length),
                ]);
            }
            return cleanedBuffer;
        };
        const buffers = [
            new Buffer(`00${typeBuf.length}`.slice(-3)),
            typeBuf,
            new Buffer(':'),
        ];
        if (typeof Key === 'string') {
            buffers.push(stringify(id));
        }
        else {
            const keys = Object.keys(Key);
            while (keys.length) {
                const key = keys.shift();
                if (typeof key === 'undefined') {
                    break;
                }
                buffers.push(stringify(id[key]));
                if (keys.length > 0) {
                    buffers.push(new Buffer([SEPARATOR]));
                }
            }
        }
        return Buffer.concat(buffers).toString(encoding);
    }
    static stringifyValues(type, Key, id, encoding = 'base64') {
        const decoded = GlobalId.stringify(type, Key, id, 'utf8');
        const [entityName, ...rest] = decoded.split(':');
        return new Buffer(rest.join(':')).toString(encoding);
    }
    static getType(globalId, encoding = 'base64') {
        GlobalId.validate(globalId, undefined, encoding);
        const buffer = new Buffer(globalId, encoding);
        const typeSize = parseInt(buffer.slice(0, 3).toString(), 10);
        return buffer.slice(3, 3 + typeSize).toString();
    }
    static parse(globalId, cast, matchType, encoding = 'base64') {
        const parseValue = (blob, cast) => {
            if (blob.length === 0) {
                if (cast.match(/\?$/) == null) {
                    throw new InvalidIDError_1.InvalidIDError(Object.assign({ encoding }, (matchType != null && {
                        matchType: Array.isArray(matchType) ? matchType : [matchType],
                    }), { message: `Empty data for id field`, id: globalId, method: 'parse' }));
                }
                return undefined;
            }
            switch (cast.replace(/\?$/, '')) {
                case 'Uuid':
                    return new Uuid_1.Uuid(blob);
                case 'TimeUuid':
                    return new TimeUuid_1.TimeUuid(blob);
                case 'DateTime':
                case 'LocalDate':
                    return DateTime_1.DateTime.fromISO(blob.toString());
                case 'Long':
                    return Long_1.Long.fromString(blob.toString());
                case 'String':
                case 'URI':
                case 'Email':
                case 'Phone':
                    return blob.toString();
                case 'Integer':
                    return parseInt(blob.toString(), 10);
                case 'Float':
                    return parseFloat(blob.toString());
                case 'Blob':
                    return blob;
                default:
                    throw new InvalidIDError_1.InvalidIDError(Object.assign({ encoding }, (matchType != null && {
                        matchType: Array.isArray(matchType) ? matchType : [matchType],
                    }), { message: `Unknown cast`, id: globalId, method: 'parse' }));
            }
        };
        const buffer = new Buffer(globalId, encoding);
        const typeSize = parseInt(buffer.slice(0, 3).toString(), 10);
        if (typeof matchType !== 'undefined') {
            const type = this.getType(globalId, encoding);
            if ((Array.isArray(matchType) && matchType.indexOf(type) < 0) || type !== matchType) {
                throw new InvalidIDError_1.InvalidIDError(Object.assign({ encoding }, (matchType != null && {
                    matchType: Array.isArray(matchType) ? matchType : [matchType],
                }), { message: `Global id type doesn't match with type`, id: globalId, method: 'parse' }));
            }
        }
        else {
            this.validate(globalId, matchType, encoding);
        }
        const blob = buffer.slice(typeSize + 4);
        if (typeof cast === 'undefined') {
            return blob;
        }
        if (typeof cast === 'string') {
            return parseValue(blob, cast);
        }
        const obj = {};
        const keys = Object.keys(cast);
        let valueCount = 0;
        let offset = 0;
        const nextValue = (index) => {
            const key = keys.shift();
            if (typeof key === 'undefined') {
                throw new InvalidIDError_1.InvalidIDError(Object.assign({ encoding }, (matchType != null && {
                    matchType: Array.isArray(matchType) ? matchType : [matchType],
                }), { message: `Too much values`, id: globalId, method: 'parse' }));
            }
            valueCount = valueCount + 1;
            obj[key] = parseValue(blob.slice(offset, index), cast[key]);
            offset = index + 1;
        };
        for (const index of blob.keys()) {
            const byte = blob[index];
            if (byte === SEPARATOR && blob[index - 1] !== ESCAPE) {
                nextValue(index);
            }
        }
        nextValue(blob.length);
        if (valueCount < keys.length) {
            throw new InvalidIDError_1.InvalidIDError(Object.assign({ encoding }, (matchType != null && {
                matchType: Array.isArray(matchType) ? matchType : [matchType],
            }), { message: `Too little values`, id: globalId, method: 'parse' }));
        }
        return obj;
    }
    static parseValues(strValues, cast, matchType = 'Unknown', encoding = 'base64') {
        const typeSize = `00${matchType.length}`.slice(-3);
        const prefix = new Buffer(`${typeSize}${matchType}:`);
        const buffer = Buffer.concat([prefix, new Buffer(strValues, encoding)]);
        return GlobalId.parse(buffer.toString('utf8'), cast, matchType === 'Unknown' ? undefined : matchType, 'utf8');
    }
    stringify(data, encoding = 'base64') {
        return GlobalId.stringify(this.entityName, this.signature, data, encoding);
    }
    stringifyValues(data, encoding = 'utf8') {
        return GlobalId.stringifyValues(this.entityName, this.signature, data, 'utf8');
    }
    parse(id, encoding = 'base64') {
        return GlobalId.parse(id, this.signature, this.entityName, encoding);
    }
    parseValues(strValues, encoding = 'utf8') {
        return GlobalId.parseValues(strValues, this.signature, this.entityName, encoding);
    }
}
GlobalId.entityName = 'node';
exports.GlobalId = GlobalId;
//# sourceMappingURL=GlobalId.js.map