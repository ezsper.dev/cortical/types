"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_server_errors_1 = require("apollo-server-errors");
class InvalidIDError extends apollo_server_errors_1.ApolloError {
    constructor(options) {
        const { message } = options, properties = __rest(options, ["message"]);
        super(message == null ? `Invalid global id` : message, 'INVALID_ID_ERROR', properties);
    }
}
exports.InvalidIDError = InvalidIDError;
//# sourceMappingURL=InvalidIDError.js.map