import { ApolloError } from 'apollo-server-errors';
interface InvalidIDErrorOptions {
    method: string;
    message?: string;
    id?: string;
    data?: any;
    matchType?: string[];
    encoding?: string;
}
export declare class InvalidIDError extends ApolloError {
    constructor(options: InvalidIDErrorOptions);
}
export {};
