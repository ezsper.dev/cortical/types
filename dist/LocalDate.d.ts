import { DateTime } from 'luxon';
export declare type LocalDate = DateTime;
