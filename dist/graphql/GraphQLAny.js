"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
function identity(value) {
    return value;
}
function parseLiteral(ast) {
    switch (ast.kind) {
        case graphql_1.Kind.STRING:
        case graphql_1.Kind.BOOLEAN:
            return ast.value;
        case graphql_1.Kind.INT:
        case graphql_1.Kind.FLOAT:
            return parseFloat(ast.value);
        case graphql_1.Kind.OBJECT: {
            const value = {}; // Object.create(null);
            ast.fields.forEach(field => {
                value[field.name.value] = parseLiteral(field.value);
            });
            return value;
        }
        case graphql_1.Kind.LIST:
            return ast.values.map(parseLiteral);
        default:
            return null;
    }
}
exports.GraphQLAny = new graphql_1.GraphQLScalarType({
    parseLiteral,
    name: 'Any',
    description: 'The `Any` represents any JSON value as specified by ' +
        '[ECMA-404](http://www.ecma-international.org/' +
        'publications/files/ECMA-ST/ECMA-404.pdf).',
    serialize: identity,
    parseValue: identity,
});
//# sourceMappingURL=GraphQLAny.js.map