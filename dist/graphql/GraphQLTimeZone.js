"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const luxon_1 = require("luxon");
const timezones = Object.keys(require('countries-and-timezones').getAllTimezones());
const lowerCasedTimezones = timezones.map(timezone => timezone.toLowerCase());
const resolveTimeZone = (value) => {
    const zoneIndex = lowerCasedTimezones.indexOf(value.toLowerCase());
    if (zoneIndex >= 0) {
        const zone = luxon_1.DateTime.local().setZone(timezones[zoneIndex]);
        if (zone.isValid) {
            return zone.zoneName;
        }
    }
    return null;
};
exports.GraphQLTimeZone = new graphql_1.GraphQLScalarType({
    name: 'TimeZone',
    description: `An IANA-specified zone.`,
    serialize(value) {
        if (typeof value !== 'string') {
            return null;
        }
        return resolveTimeZone(value);
    },
    parseValue(value) {
        const timeZone = resolveTimeZone(value);
        if (timeZone != null) {
            return timeZone;
        }
        throw new graphql_1.GraphQLError('', []);
    },
    parseLiteral(ast) {
        if (ast.kind !== graphql_1.Kind.STRING) {
            throw new graphql_1.GraphQLError(`Type should be "String", found ${ast.kind}.`, [ast]);
        }
        const timeZone = resolveTimeZone(ast.value);
        if (timeZone != null) {
            return timeZone;
        }
        throw new graphql_1.GraphQLError(`Invalid TimeZone literal.\n${ast.value} is not a valid IANA-specified zone.`, [ast]);
    }
});
//# sourceMappingURL=GraphQLTimeZone.js.map