import { GraphQLScalarType } from 'graphql';
import { DateTimeOptions } from 'luxon';
export declare function createScalar(dateOptions?: DateTimeOptions): GraphQLScalarType;
export declare const GraphQLInterval: GraphQLScalarType;
