"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const TimeUuid_1 = require("../TimeUuid");
function parseValue(value) {
    if (value instanceof TimeUuid_1.TimeUuid) {
        return value;
    }
    if (typeof value === 'string') {
        try {
            return TimeUuid_1.TimeUuid.fromString(value);
        }
        catch (error) {
            throw new graphql_1.GraphQLError(error.message);
        }
    }
    throw new graphql_1.GraphQLError('Invalid uuid');
}
exports.GraphQLTimeUuid = new graphql_1.GraphQLScalarType({
    name: 'TimeUuid',
    description: 'A 128-bit number used to uniquely identify some object or entity',
    serialize(value) {
        return TimeUuid_1.TimeUuid.toString();
    },
    parseValue,
    parseLiteral(valueNode) {
        if (valueNode.kind === graphql_1.Kind.STRING) {
            return parseValue(valueNode.value);
        }
        return null;
    },
});
//# sourceMappingURL=GraphQLTimeUuid.js.map