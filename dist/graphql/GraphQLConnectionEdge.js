"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const GraphQLNode_1 = require("./GraphQLNode");
exports.GraphQLConnectionEdge = new graphql_1.GraphQLInterfaceType({
    name: 'ConnectionEdge',
    description: 'The standardized Relay object for dealing with pagination edges on Graph',
    fields: {
        cursor: {
            type: new graphql_1.GraphQLNonNull(graphql_1.GraphQLString),
            description: "The cursor of the edge",
            args: {},
        },
        node: {
            type: GraphQLNode_1.GraphQLNode,
            description: "The connection edges",
            args: {},
        },
    },
});
//# sourceMappingURL=GraphQLConnectionEdge.js.map