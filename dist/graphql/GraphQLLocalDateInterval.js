"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const Interval_1 = require("../Interval");
const GraphQLLocalDate_1 = require("./GraphQLLocalDate");
function parseValue(value, dateOptions) {
    const options = Object.assign({ zone: 'UTC', locale: 'en-US' }, dateOptions);
    let interval;
    try {
        if (value instanceof Interval_1.Interval) {
            interval = value;
        }
        else if (Array.isArray(value) && value.length === 2) {
            interval = Interval_1.Interval.fromDateTimes(GraphQLLocalDate_1.parseValue(value[0], options, false), GraphQLLocalDate_1.parseValue(value[1], options, true));
        }
        else if (typeof value === 'string') {
            interval = Interval_1.Interval.fromISO(value, options);
        }
    }
    catch (error) {
        throw new graphql_1.GraphQLError(error.message);
    }
    if (interval == null || !interval.isValid) {
        throw new graphql_1.GraphQLError('Invalid Interval');
    }
    return interval;
}
function createScalar(dateOptions) {
    const options = Object.assign({ zone: 'UTC', locale: 'en-US' }, dateOptions);
    const { zone, locale } = options;
    return new graphql_1.GraphQLScalarType({
        name: 'LocalDateInterval',
        description: 'An ISO-8601 interval string.',
        serialize: value => {
            let interval;
            if (typeof value === 'string') {
                interval = parseValue(value, options);
            }
            else {
                interval = value;
            }
            if (interval) {
                return [
                    interval.start.setZone(zone).setLocale(locale).toISO().replace(/(T| )([0-9.:]+)/g, ''),
                    interval.end.setZone(zone).setLocale(locale).toISO().replace(/(T| )([0-9.:]+)/g, ''),
                ];
            }
            return null;
        },
        parseValue: (value) => parseValue(value, options),
        parseLiteral(valueNode) {
            if (valueNode.kind === graphql_1.Kind.STRING) {
                return parseValue(valueNode.value, options);
            }
            else if (valueNode.kind === graphql_1.Kind.INT) {
                return parseValue(valueNode.value, options);
            }
            else if (valueNode.kind === graphql_1.Kind.LIST) {
                return parseValue(valueNode.values.map(valueNode => 'value' in valueNode ? valueNode.value : null), options);
            }
            return null;
        },
    });
}
exports.createScalar = createScalar;
exports.GraphQLLocalDateInterval = createScalar();
//# sourceMappingURL=GraphQLLocalDateInterval.js.map