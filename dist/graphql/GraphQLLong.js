"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const lodash_1 = require("lodash");
const Long_1 = require("../Long");
function parseLong(value) {
    if (value instanceof Long_1.Long) {
        return value;
    }
    if (lodash_1.isString(value)) {
        const matches = value.match(/^\s*([0-9]+)\s*$/);
        if (matches) {
            return Long_1.Long.fromString(matches[0]);
        }
    }
    return Long_1.Long.fromInt(value);
}
exports.GraphQLLong = new graphql_1.GraphQLScalarType({
    name: 'Long',
    description: 'A 64-bit signed integer, known as Long',
    serialize: value => {
        const parsedValue = parseLong(value);
        const str1 = parsedValue.toString();
        const str2 = "" + parsedValue.toNumber();
        if (str1 !== str2) {
            return str1;
        }
        return str2 - 0;
    },
    parseValue: parseLong,
    parseLiteral(valueNode) {
        if (valueNode.kind === graphql_1.Kind.STRING || valueNode.kind === graphql_1.Kind.INT) {
            return parseLong(valueNode.value);
        }
        return null;
    }
});
//# sourceMappingURL=GraphQLLong.js.map