"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const Duration_1 = require("../Duration");
function parseValue(value) {
    if (value instanceof Duration_1.Duration) {
        return value;
    }
    try {
        if (typeof value === 'number'
            || (typeof value === 'string' && /^[0-9]+$/.test(value))) {
            return Duration_1.Duration.fromMillis(typeof value === 'string'
                ? parseInt(value, 10)
                : value);
        }
        else if (typeof value === 'string') {
            return Duration_1.Duration.fromISO(value);
        }
    }
    catch (error) {
        throw new graphql_1.GraphQLError(error.message);
    }
    throw new graphql_1.GraphQLError('Invalid Duration');
}
exports.GraphQLDuration = new graphql_1.GraphQLScalarType({
    name: 'Duration',
    description: 'An ISO-8601 duration string.',
    serialize(value) {
        return value.toString();
    },
    parseValue,
    parseLiteral(valueNode) {
        if (valueNode.kind === graphql_1.Kind.STRING) {
            return parseValue(valueNode.value);
        }
        else if (valueNode.kind === graphql_1.Kind.INT) {
            return parseValue(valueNode.value);
        }
        return null;
    },
});
//# sourceMappingURL=GraphQLDuration.js.map