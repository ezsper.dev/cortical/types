"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const Interval_1 = require("../Interval");
const GraphQLDateTime_1 = require("./GraphQLDateTime");
function parseValue(value, dateOptions) {
    const options = Object.assign({ zone: 'UTC', locale: 'en-US' }, dateOptions);
    let interval;
    try {
        if (value instanceof Interval_1.Interval) {
            interval = value;
        }
        else if (Array.isArray(value) && value.length === 2) {
            interval = Interval_1.Interval.fromDateTimes(GraphQLDateTime_1.parseValue(value[0], options, false), GraphQLDateTime_1.parseValue(value[1], options, true));
        }
        else if (typeof value === 'string') {
            interval = Interval_1.Interval.fromISO(value, options);
        }
    }
    catch (error) {
        throw new graphql_1.GraphQLError(error.message);
    }
    if (interval == null || !interval.isValid) {
        throw new graphql_1.GraphQLError('Invalid Interval');
    }
    return interval;
}
function createScalar(dateOptions) {
    const options = Object.assign({ zone: 'UTC', locale: 'en-US' }, dateOptions);
    const { zone, locale } = options;
    return new graphql_1.GraphQLScalarType({
        name: 'Interval',
        description: 'An ISO-8601 interval string.',
        serialize: value => {
            const parsedValue = parseValue(value, options);
            if (parsedValue) {
                return Interval_1.Interval.fromDateTimes(parsedValue.start.setZone(zone).setLocale(locale), parsedValue.end.setZone(zone).setLocale(locale)).toISO();
            }
            return null;
        },
        parseValue: (value) => parseValue(value, options),
        parseLiteral(valueNode) {
            if (valueNode.kind === graphql_1.Kind.STRING) {
                return parseValue(valueNode.value, options);
            }
            else if (valueNode.kind === graphql_1.Kind.INT) {
                return parseValue(valueNode.value, options);
            }
            else if (valueNode.kind === graphql_1.Kind.LIST) {
                return parseValue(valueNode.values.map(valueNode => 'value' in valueNode ? valueNode.value : null), options);
            }
            return null;
        },
    });
}
exports.createScalar = createScalar;
exports.GraphQLInterval = createScalar();
//# sourceMappingURL=GraphQLInterval.js.map