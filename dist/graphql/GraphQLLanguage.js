"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const resolveLanguage = (value) => {
    let lang;
    let country;
    let match = value.match(/^([a-z]{2})([A-Z]{2})$/);
    if (match != null) {
        lang = match[1];
        country = match[2];
    }
    else {
        match = value.match(/^((?:[a-z]{2}|[A-Z]{2}))(?:(?:-|_)(?:([a-z]{2}|[A-Z]{2})))?$/);
        if (match != null) {
            lang = match[1].toLowerCase();
            country = match[2] != null
                ? match[2].toUpperCase()
                : undefined;
        }
        else {
            return null;
        }
    }
    if (!require('iso-639-1').validate(lang)) {
        return null;
    }
    if (country != null && !require('i18n-iso-countries').isValid(country)) {
        return null;
    }
    return country == null ? `${lang}` : `${lang}_${country}`;
};
exports.GraphQLLanguage = new graphql_1.GraphQLScalarType({
    name: 'Language',
    description: `An ISO-639 Language Code and ISO-3166 Country Code.`,
    serialize(value) {
        if (typeof value !== 'string') {
            return null;
        }
        return resolveLanguage(value);
    },
    parseValue(value) {
        const languageCode = resolveLanguage(value);
        if (languageCode != null) {
            return languageCode;
        }
        throw new graphql_1.GraphQLError('', []);
    },
    parseLiteral(ast) {
        if (ast.kind !== graphql_1.Kind.STRING) {
            throw new graphql_1.GraphQLError(`Type should be "String", found ${ast.kind}.`, [ast]);
        }
        const languageCode = resolveLanguage(ast.value);
        if (languageCode != null) {
            return languageCode;
        }
        throw new graphql_1.GraphQLError(`Invalid Language literal.\n${ast.value} is not valid Language.`, [ast]);
    }
});
//# sourceMappingURL=GraphQLLanguage.js.map