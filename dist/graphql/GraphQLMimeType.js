"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const isMimeType = require("validator/lib/isMimeType");
exports.GraphQLMimeType = new graphql_1.GraphQLScalarType({
    name: 'MimeType',
    description: `An RFC 2425 compliant Mime type string.`,
    serialize(value) {
        if (typeof value !== 'string') {
            return null;
        }
        if (isMimeType(value)) {
            return value;
        }
        return null;
    },
    parseValue(value) {
        if (typeof value !== 'string') {
            throw new graphql_1.GraphQLError('', []);
        }
        if (isMimeType(value)) {
            return value;
        }
        throw new graphql_1.GraphQLError('', []);
    },
    parseLiteral(ast) {
        if (ast.kind !== graphql_1.Kind.STRING) {
            throw new graphql_1.GraphQLError(`Type should be "String", found ${ast.kind}.`, [ast]);
        }
        if (isMimeType(ast.value)) {
            return ast.value;
        }
        throw new graphql_1.GraphQLError(`Invalid MimeType literal.\n${ast.value} is not MimeType.`, [ast]);
    }
});
//# sourceMappingURL=GraphQLMimeType.js.map