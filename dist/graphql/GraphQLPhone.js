"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const isMobilePhone = require("validator/lib/isMobilePhone");
exports.GraphQLPhone = new graphql_1.GraphQLScalarType({
    name: 'Phone',
    description: `An RFC 3191 and RFC 3192 compliant Phone string`,
    serialize(value) {
        if (typeof value === 'string' && value.charAt(0) !== '+') {
            if (isMobilePhone(value, 'any')) {
                return value;
            }
        }
        return null;
    },
    parseValue(value) {
        if (typeof value !== 'string') {
            throw new graphql_1.GraphQLError('', []);
        }
        if (isMobilePhone(value, 'any')) {
            return value;
        }
        throw new graphql_1.GraphQLError('', []);
    },
    parseLiteral(ast) {
        if (ast.kind !== graphql_1.Kind.STRING) {
            throw new graphql_1.GraphQLError(`Type should be "String", found ${ast.kind}.`, [ast]);
        }
        if (ast.value.charAt(0) !== '+') {
            throw new graphql_1.GraphQLError(`Invalid Mobile Phone literal.\nMust start with "+" followed by country code.`, [ast]);
        }
        if (isMobilePhone(ast.value, 'any')) {
            return ast.value;
        }
        throw new graphql_1.GraphQLError(`Invalid Mobile Phone literal.\n${ast.value} is not a valid Mobile Phone.`, [ast]);
    }
});
//# sourceMappingURL=GraphQLPhone.js.map