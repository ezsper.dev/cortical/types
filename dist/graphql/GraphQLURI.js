"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const isURL = require("validator/lib/isURL");
exports.GraphQLURI = new graphql_1.GraphQLScalarType({
    name: 'URI',
    description: `An RFC 3986, RFC 3987, and RFC 6570 (level 4) compliant URI string.`,
    serialize(value) {
        if (typeof value !== 'string') {
            return null;
        }
        if (isURL(value)) {
            return value;
        }
        return null;
    },
    parseValue(value) {
        if (typeof value !== 'string') {
            throw new graphql_1.GraphQLError('', []);
        }
        if (isURL(value)) {
            return value;
        }
        throw new graphql_1.GraphQLError('', []);
    },
    parseLiteral(ast) {
        if (ast.kind !== graphql_1.Kind.STRING) {
            throw new graphql_1.GraphQLError(`Type should be "String", found ${ast.kind}.`, [ast]);
        }
        if (isURL(ast.value)) {
            return ast.value;
        }
        throw new graphql_1.GraphQLError(`Invalid URL literal.\n${ast.value} is not URL.`, [ast]);
    }
});
//# sourceMappingURL=GraphQLURI.js.map