"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const resolveCountry = (value) => {
    if (value.length === 2) {
        if (require('i18n-iso-countries').alpha2ToAlpha3(value.toUpperCase()) != null) {
            return value.toUpperCase();
        }
    }
    else if (value.length === 3) {
        return require('i18n-iso-countries').alpha3ToAlpha2(value.toUpperCase());
    }
    return require('i18n-iso-countries').getAlpha2Code(value, 'en') || null;
};
exports.GraphQLCountry = new graphql_1.GraphQLScalarType({
    name: 'Country',
    description: `An ISO-3166 Country Code.'`,
    serialize(value) {
        if (typeof value !== 'string') {
            return null;
        }
        return resolveCountry(value);
    },
    parseValue(value) {
        const countryCode = resolveCountry(value);
        if (countryCode != null) {
            return countryCode;
        }
        throw new graphql_1.GraphQLError('', []);
    },
    parseLiteral(ast) {
        if (ast.kind !== graphql_1.Kind.STRING) {
            throw new graphql_1.GraphQLError(`Type should be "String", found ${ast.kind}.`, [ast]);
        }
        const countryCode = resolveCountry(ast.value);
        if (countryCode != null) {
            return countryCode;
        }
        throw new graphql_1.GraphQLError(`Invalid Country literal.\n${ast.value} is not valid Country.`, [ast]);
    }
});
//# sourceMappingURL=GraphQLCountry.js.map