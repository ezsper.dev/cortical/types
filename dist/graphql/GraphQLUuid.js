"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const Uuid_1 = require("../Uuid");
function parseValue(value) {
    if (value instanceof Uuid_1.Uuid) {
        return value;
    }
    if (typeof value === 'string') {
        try {
            return Uuid_1.Uuid.fromString(value);
        }
        catch (error) {
            throw new graphql_1.GraphQLError(error.message);
        }
    }
    throw new graphql_1.GraphQLError('Invalid uuid');
}
exports.GraphQLUuid = new graphql_1.GraphQLScalarType({
    name: 'Uuid',
    description: 'A 128-bit number used to uniquely identify some object or entity',
    serialize(value) {
        return value.toString();
    },
    parseValue,
    parseLiteral(valueNode) {
        if (valueNode.kind === graphql_1.Kind.STRING) {
            return parseValue(valueNode.value);
        }
        return null;
    },
});
//# sourceMappingURL=GraphQLUuid.js.map