"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
exports.GraphQLConnectionPageInfo = new graphql_1.GraphQLObjectType({
    name: 'ConnectionPageInfo',
    description: 'The standardized Relay object for dealing with pagination info on Graph',
    fields: {
        startCursor: {
            type: graphql_1.GraphQLString,
            description: "The start cursor of the connection",
            args: {},
        },
        hasPreviousPage: {
            type: graphql_1.GraphQLBoolean,
            description: "If has a previous page in the connection position",
            args: {},
        },
        hasNextPage: {
            type: graphql_1.GraphQLBoolean,
            description: "If has a next page in the connection position",
            args: {},
        },
        endCursor: {
            type: graphql_1.GraphQLString,
            description: "The end cursor of the connection",
            args: {},
        },
    },
});
//# sourceMappingURL=GraphQLConnectionPageInfo.js.map