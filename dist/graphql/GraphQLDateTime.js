"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const DateTime_1 = require("../DateTime");
function parseValue(value, dateOptions, end = false) {
    const options = Object.assign({ zone: 'UTC', locale: 'en-US' }, dateOptions);
    let date;
    try {
        if (value instanceof DateTime_1.DateTime) {
            date = value;
        }
        else if (value instanceof Date) {
            date = DateTime_1.DateTime.fromJSDate(value, options);
        }
        else if (typeof value === 'string') {
            const isoString = !/(T| )([0-9.:]+)/.test(value)
                ? value.replace(/^([0-9-]+)/, end ? '$1T23:59:59' : '$1T00:00:00')
                : value;
            date = DateTime_1.DateTime.fromISO(isoString, options);
        }
        else if (typeof value === 'number') {
            date = DateTime_1.DateTime.fromMillis(value, options);
        }
    }
    catch (error) {
        throw new graphql_1.GraphQLError(error.message);
    }
    if (date == null || !date.isValid) {
        throw new graphql_1.GraphQLError('Invalid DateTime');
    }
    return date;
}
exports.parseValue = parseValue;
function createScalar(dateOptions) {
    const options = Object.assign({ zone: 'UTC', locale: 'en-US' }, dateOptions);
    const { zone, locale } = options;
    return new graphql_1.GraphQLScalarType({
        name: 'DateTime',
        description: 'An ISO-8601 encoded UTC date string (without time component)',
        serialize: value => {
            const parsedValue = parseValue(value);
            if (parsedValue) {
                return parsedValue.setZone(zone).setLocale(locale).toISO();
            }
            return null;
        },
        parseValue: (value) => parseValue(value, options),
        parseLiteral(valueNode) {
            if (valueNode.kind === graphql_1.Kind.STRING) {
                return parseValue(valueNode.value, options);
            }
            else if (valueNode.kind === graphql_1.Kind.INT) {
                return parseValue(valueNode.value, options);
            }
            return null;
        }
    });
}
exports.createScalar = createScalar;
exports.GraphQLDateTime = createScalar();
//# sourceMappingURL=GraphQLDateTime.js.map