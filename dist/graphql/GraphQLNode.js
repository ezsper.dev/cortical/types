"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
exports.GraphQLNode = new graphql_1.GraphQLInterfaceType({
    name: 'Node',
    description: 'The stardized global type fetching',
    fields: {
        id: {
            type: new graphql_1.GraphQLNonNull(graphql_1.GraphQLID),
            description: "The node's global ID",
            args: {},
        }
    },
});
//# sourceMappingURL=GraphQLNode.js.map