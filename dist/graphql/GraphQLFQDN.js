"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const validator_1 = require("validator");
exports.GraphQLFQDN = new graphql_1.GraphQLScalarType({
    name: 'FQDN',
    description: `An RFC 4703 compliant FQDN string.'`,
    serialize(value) {
        if (typeof value !== 'string') {
            return null;
        }
        if (validator_1.isFQDN(value)) {
            return value;
        }
        return null;
    },
    parseValue(value) {
        if (typeof value !== 'string') {
            throw new graphql_1.GraphQLError('', []);
        }
        if (validator_1.isFQDN(value)) {
            return value;
        }
        throw new graphql_1.GraphQLError('', []);
    },
    parseLiteral(ast) {
        if (ast.kind !== graphql_1.Kind.STRING) {
            throw new graphql_1.GraphQLError(`Type should be "String", found ${ast.kind}.`, [ast]);
        }
        if (validator_1.isFQDN(ast.value)) {
            return ast.value;
        }
        throw new graphql_1.GraphQLError(`Invalid FQDN literal.\n${ast.value} is not FQDN.`, [ast]);
    }
});
//# sourceMappingURL=GraphQLFQDN.js.map