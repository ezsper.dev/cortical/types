"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const DateTime_1 = require("../DateTime");
const millisecondsPerDay = 86400000;
function parseValue(value, dateOptions, end = false) {
    if (value instanceof DateTime_1.DateTime) {
        return value;
    }
    const options = Object.assign({ zone: 'UTC', locale: 'en-US' }, dateOptions);
    if (value instanceof Date) {
        return DateTime_1.DateTime.fromJSDate(value, options);
    }
    try {
        if (typeof value === 'string') {
            const isoString = !/(T| )([0-9.:]+)/.test(value)
                ? value.replace(/^([0-9-]+)/, end ? '$1T23:59:59' : '$1T00:00:00')
                : value;
            return DateTime_1.DateTime.fromISO(isoString, options);
        }
        else if (typeof value === 'number') {
            if (value < -2147483648 || value > 2147483647) {
                throw new Error('You must provide a valid value for days since epoch (-2147483648 <= value <= 2147483647).');
            }
            const date = new Date(value * millisecondsPerDay);
            const year = date.getUTCFullYear();
            const month = date.getUTCMonth() + 1;
            const day = date.getUTCDate();
            const { zone, locale } = options;
            return DateTime_1.DateTime.fromObject({ year, month, day, zone, locale });
        }
    }
    catch (error) {
        throw new graphql_1.GraphQLError(error.message);
    }
    throw new graphql_1.GraphQLError('Invalid local date');
}
exports.parseValue = parseValue;
function createScalar(dateOptions) {
    const options = Object.assign({ zone: 'UTC', locale: 'en-US' }, dateOptions);
    const { zone, locale } = options;
    return new graphql_1.GraphQLScalarType({
        name: 'LocalDate',
        description: 'An ISO-8601 encoded UTC date string (without time component)',
        serialize(value) {
            let date;
            if (typeof value === 'string') {
                date = parseValue(value, options);
            }
            else {
                date = value;
            }
            return date.setZone(zone).setLocale(locale).toISO().replace(/(T| )([0-9.:]+)/, '');
        },
        parseValue: (value) => parseValue(value, options),
        parseLiteral(valueNode) {
            if (valueNode.kind === graphql_1.Kind.STRING) {
                return parseValue(valueNode.value, options);
            }
            return null;
        },
    });
}
exports.createScalar = createScalar;
exports.GraphQLLocalDate = createScalar();
//# sourceMappingURL=GraphQLLocalDate.js.map