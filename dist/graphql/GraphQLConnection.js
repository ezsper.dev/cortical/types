"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const graphql_1 = require("graphql");
const GraphQLConnectionEdge_1 = require("./GraphQLConnectionEdge");
const GraphQLConnectionPageInfo_1 = require("./GraphQLConnectionPageInfo");
const GraphQLNode_1 = require("./GraphQLNode");
exports.GraphQLConnection = new graphql_1.GraphQLInterfaceType({
    name: 'Connection',
    description: 'The standardized Relay object for dealing with pagination on Graph',
    fields: {
        edges: {
            type: new graphql_1.GraphQLList(GraphQLConnectionEdge_1.GraphQLConnectionEdge),
            description: "The connection edges",
            args: {},
        },
        nodes: {
            type: new graphql_1.GraphQLList(GraphQLNode_1.GraphQLNode),
            description: "The connection nodes",
            args: {},
        },
        pageInfo: {
            type: new graphql_1.GraphQLNonNull(GraphQLConnectionPageInfo_1.GraphQLConnectionPageInfo),
            description: "The connection page info",
            args: {},
        },
    },
});
//# sourceMappingURL=GraphQLConnection.js.map