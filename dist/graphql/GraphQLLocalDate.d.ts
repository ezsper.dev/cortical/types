import { GraphQLScalarType } from 'graphql';
import { DateTimeOptions } from 'luxon';
import { DateTime } from '../DateTime';
export declare function parseValue(value: any, dateOptions?: DateTimeOptions, end?: boolean): DateTime;
export declare function createScalar(dateOptions?: DateTimeOptions): GraphQLScalarType;
export declare const GraphQLLocalDate: GraphQLScalarType;
